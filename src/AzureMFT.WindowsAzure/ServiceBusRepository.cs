﻿using AzureMFT.Common.Configuration;
using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Repositories;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.WindowsAzure
{
    public class ServiceBusRepository : IRelayRepository
    {
        private readonly QueueClient _queueClient;
        private readonly QueueClient _as2FileReceivedQueueClient;
        private readonly QueueClient _as2DispatchQueueClient;

        public ServiceBusRepository(ConfigurationManager config, ISecretConfigurationManager secretConfig)
        {
            _queueClient = QueueClient.CreateFromConnectionString(secretConfig.GetSecret(Common.Models.Secrets.Names.RelaySBConnectionString), config.RelayQueueName);
            _as2FileReceivedQueueClient = QueueClient.CreateFromConnectionString(secretConfig.GetSecret(Common.Models.Secrets.Names.RelaySBConnectionString), config.AS2FileReceivedQueueName);
            _as2DispatchQueueClient = QueueClient.CreateFromConnectionString(secretConfig.GetSecret(Common.Models.Secrets.Names.RelaySBConnectionString), config.AS2MDNDispatchQueueName);
        }

        public Task QueueRelayMessageAsync(RelayMessage queueMessage)
        {
            var message = new BrokeredMessage(queueMessage);

            if (_queueClient == null)
                return null;

            return _queueClient.SendAsync(message);
        }

        public Task QueueAS2FileReceivedMessageAsync(RelayMessage queueMessage)
        {
            var message = new BrokeredMessage(queueMessage);

            if (_as2FileReceivedQueueClient == null)
                return null;

            return _as2FileReceivedQueueClient.SendAsync(message);
        }

        public Task QueueAS2MDNMessageAsync(AS2MessageDispositionNotice queueMessage)
        {
            var message = new BrokeredMessage(queueMessage);

            if (_as2DispatchQueueClient == null)
                return null;

            return _as2DispatchQueueClient.SendAsync(message);
        }

        public BrokeredMessage Receive()
        {
            return _queueClient.Receive();
        }

        public BrokeredMessage Peek()
        {
            return _queueClient.Peek();
        }
    }
}
