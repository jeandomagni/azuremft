﻿using AzureMFT.Common;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Result;
using AzureMFT.Common.Validation;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureMFT.WindowsAzure
{
    public class AzureFileSystemRepository : IFileSystemRepository
    {
        private readonly CloudStorageAccount _account;
        private readonly ILoggingService _logger;

        public AzureFileSystemRepository(ISecretConfigurationManager config, ILoggingService logger) :
            this(CloudStorageAccount.Parse(config.GetSecret(Secrets.Names.StorageConnectionString)))
        {
            _logger = logger;
        }

        public AzureFileSystemRepository(CloudStorageAccount storageAccount)
        {
            _account = storageAccount;
        }

        public List<PathListingItem> GetPathListing(string path, IAgreement agreement)
        {
            var container = GetAgreementContainer(agreement);

            var resolvedPath = ResolvePath(path, agreement);

            var blobs = container.ListBlobs(resolvedPath);

            return ToPathListingItems(blobs).OrderBy(x => (int)x.ItemType).ThenBy(x => x.Name).ToList();
        }

        private string ResolvePath(string path, IAgreement agreement)
        {
            if (!string.IsNullOrWhiteSpace(agreement.TradingPartner.TradingPartnerName))
                return $"{agreement.AgreementName}/{path}";

            return path;
        }

        private string ResolvePath(string path, string subFolder, IAgreement agreement)
        {
            if (!string.IsNullOrWhiteSpace(agreement.TradingPartner.TradingPartnerName))
                return $"{agreement.AgreementName}/{subFolder}/{path}";

            return path;
        }

        private List<PathListingItem> ToPathListingItems(IEnumerable<IListBlobItem> blobs)
        {
            var result = new List<PathListingItem>();
            foreach (var blob in blobs)
            {
                var pathListingItem = ToPathListingItem(blob);
                if (pathListingItem != null)
                    result.Add(pathListingItem);
            }
            return result;
        }

        private PathListingItem ToPathListingItem(IListBlobItem item)
        {
            PathListingItem listingItem = null;

            if (item is CloudBlob)
            {
                var blob = (CloudBlob)item;
                if (!blob.Name.EndsWith("directory.hidden"))
                {
                    listingItem = new PathListingItem
                    {
                        ItemType = PathListingItemType.File,
                        Name = System.IO.Path.GetFileName(blob.Name),
                        LastModifiedUtcDate = blob.Properties.LastModified.GetValueOrDefault().UtcDateTime,
                        Size = blob.Properties.Length
                    };
                }
            }
            else if (item is CloudBlobDirectory)
            {
                var directory = (CloudBlobDirectory)item;
                listingItem = new PathListingItem
                {
                    ItemType = PathListingItemType.Directory,
                    Name = directory.Uri.Segments.Last().Replace("/", string.Empty)
                };
            }

            return listingItem;
        }

        public PathListingItem GetFileAttributes(string path, IAgreement agreement)
        {
            var container = GetAgreementContainer(agreement);
            var resolvedPath = ResolvePath(path, agreement);

            try
            {
                if (path == Path.GetTempPath())
                    return new PathListingItem() { ItemType = PathListingItemType.Directory };

                CloudBlob blob = container.GetBlobReference(resolvedPath);
                CloudBlobDirectory directory = container.GetDirectoryReference(resolvedPath);
                var directoryFake = directory.GetBlobReference("directory.hidden");
                if (directoryFake.Exists())
                    return new PathListingItem() { ItemType = PathListingItemType.Directory };
                else if(blob.Exists())
                    return ToPathListingItem(blob);
                else
                {
                    return null;
                }
            }
            catch (StorageException se)
            {
                if (se.RequestInformation.HttpStatusCode == (int)System.Net.HttpStatusCode.NotFound)
                    return null;
                else
                    throw;
            }
        }

        public GenericResult<Stream> GetDownloadStream(string path, IAgreement agreement)
        {
            var result = new GenericResult<Stream>();
            var resolvedPath = ResolvePath(path, agreement);

            var blob = GetAgreementContainer(agreement).GetBlockBlobReference(resolvedPath);

            try
            {
                result.Payload = blob.OpenRead();
            }
            catch (StorageException se)
            {
                if (se.RequestInformation.HttpStatusCode == (int)System.Net.HttpStatusCode.NotFound)
                {
                    result.AddViolation("sourcePath", "Path could not be found.");
                }
                else
                {
                    _logger.LogException(se);
                    result.AddViolation("sourcePath", "An unspecified error occurred.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogException(ex);
                result.AddViolation("sourcePath", "An unspecified error occurred.");
            }

            return result;
        }

        public GenericResult<Stream> GetUploadStream(string path, IAgreement agreement)
        {
            var result = new GenericResult<Stream>(ValidateFilePath(path));
            var resolvedPath = ResolvePath(path, agreement);

            if (result.IsValid)
            {
                var blob = GetAgreementContainer(agreement).GetBlockBlobReference(resolvedPath);

                try
                {
                    result.Payload = blob.OpenWrite();
                }
                catch (Exception ex)
                {
                    _logger.LogException(ex);
                    result.AddViolation("sourcePath", "An unspecified error occurred.");
                }
            }

            return result;
        }

        public GenericResult<Stream> GetUploadStream(string path, string subFolder, IAgreement agreement)
        {
            var result = new GenericResult<Stream>(ValidateFilePath(path));
            var resolvedPath = ResolvePath(path, subFolder, agreement);

            if (result.IsValid)
            {
                var blob = GetAgreementContainer(agreement).GetBlockBlobReference(resolvedPath);

                try
                {
                    result.Payload = blob.OpenWrite();
                }
                catch (Exception ex)
                {
                    _logger.LogException(ex);
                    result.AddViolation("sourcePath", "An unspecified error occurred.");
                }
            }

            return result;
        }

        public ValidationResult RenameFile(string sourcePath, string targetPath, IAgreement agreement)
        {
            var result = ValidateRenameDoesNotAttemptDirectoryCreation(sourcePath, targetPath);
            if (!result.IsValid)
                return result;

            result = ValidateFilePath(targetPath);

            if (result.IsValid)
            {
                var container = GetAgreementContainer(agreement);
                var resolvedSourcePath = ResolvePath(sourcePath, agreement);
                var resolvedTargetPath = ResolvePath(targetPath, agreement);

                var sourceBlob = container.GetBlockBlobReference(resolvedSourcePath);
                var targetBlob = container.GetBlockBlobReference(resolvedTargetPath);

                try
                {
                    targetBlob.StartCopy(sourceBlob);
                    while (true)
                    {
                        targetBlob.FetchAttributes();
                        if (targetBlob.CopyState.Status != CopyStatus.Pending)
                            break;

                        Thread.Sleep(1000);
                    }

                    sourceBlob.Delete();

                }
                catch (StorageException se)
                {
                    if (se.RequestInformation.HttpStatusCode == (int)System.Net.HttpStatusCode.NotFound)
                    {
                        result = new GenericResult<FileSystemCodes>();
                        result.AddViolation("sourcePath", "Source path cannot not be renamed.");
                    }
                    else
                    {
                        _logger.LogException(se);
                        result.AddViolation("sourcePath", "An unspecified error occurred.");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogException(ex);
                    result.AddViolation("sourcePath", "An unspecified error occurred.");
                }
            }

            return result;
        }

        public ValidationResult DeleteFile(string path, IAgreement agreement)
        {
            var result = new ValidationResult();
            var container = GetAgreementContainer(agreement);
            var resolvedPath = ResolvePath(path, agreement);

            var blob = container.GetBlobReference(resolvedPath);

            try
            {
                blob.DeleteIfExists();
            }
            catch(Exception ex)
            {
                _logger.LogException(ex);
                result.AddViolation("path", "An unspecified error occurred.");
            }

            return result;
        }

        private ValidationResult ValidateFilePath(string targetPath)
        {
            var result = new ValidationResult();

            var attemptedFileName = Path.GetFileName(targetPath);
            if (attemptedFileName.EndsWith("."))
            {
                result.RuleViolations.Add(new RuleViolation("targetPath", $"File name cannot end with a period (.)"));
                return result;
            }

            if(attemptedFileName.Contains("\\"))
            {
                result.RuleViolations.Add(new RuleViolation("targetPath", $"File name cannot contain a backslash (\\)"));
                return result;
            }

            if(targetPath.Length > 1000)
            {
                result.RuleViolations.Add(new RuleViolation("targetPath", $"The file name is too long."));
                return result;
            }

            return result;
        }

        private ValidationResult ValidateRenameDoesNotAttemptDirectoryCreation(string sourcePath, string targetPath)
        {
            var result = new ValidationResult();
            // Verify this is not creating a new directory
            if (Path.GetDirectoryName(sourcePath) != Path.GetDirectoryName(targetPath))
            {
                var slashedFileName = targetPath.Replace(Path.GetDirectoryName(sourcePath), string.Empty);
                result.RuleViolations.Add(new RuleViolation("targetPath", $"Invalid character / in file name."));
            }

            return result;
        }

        private CloudBlobContainer GetAgreementContainer(IAgreement agreement)
        {
            CloudBlobContainer container = null;
            if (!string.IsNullOrWhiteSpace(agreement.TradingPartner.TradingPartnerName))
                container = GetBlobClient().GetContainerReference(agreement.TradingPartner.TradingPartnerName.ToLower());
            else
                container = GetBlobClient().GetContainerReference(agreement.AgreementName);

            container.CreateIfNotExists();

            return container;
        }

        /// <summary>
        /// Returns a pre-configured CloudBlobClient
        /// </summary>
        private CloudBlobClient GetBlobClient()
        {
            CloudBlobClient client = _account.CreateCloudBlobClient();
            client.DefaultRequestOptions.RetryPolicy = new ExponentialRetry(TimeSpan.FromSeconds(2.8), 3);
            client.DefaultRequestOptions.ServerTimeout = TimeSpan.FromMinutes(6);

            return client;
        }
    }
}
