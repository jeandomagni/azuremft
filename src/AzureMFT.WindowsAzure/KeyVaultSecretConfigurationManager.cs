﻿using AzureMFT.Common.Configuration;
using AzureMFT.Common.Models;
using AzureMFT.Common.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.WindowsAzure
{
    public class KeyVaultSecretConfigurationManager : ISecretConfigurationManager
    {
        private static object _secretsLock = new object();
        private readonly ISecretVaultRepository _secretVaultRepository;

        public KeyVaultSecretConfigurationManager(ISecretVaultRepository secretVaultRepository)
        {
            _secretVaultRepository = secretVaultRepository;
        }


        private Secrets _secrets;
        public string GetSecret(Secrets.Names secret)
        {
            if (_secrets == null)
                lock (_secretsLock)
                    if (_secrets == null)
                        _secrets = _secretVaultRepository.GetSecrets();

            return _secrets.Get(secret);
        }
    }
}
