﻿using Autofac;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.WindowsAzure
{
    public class RegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<KeyVaultRepository>().As<ISecretVaultRepository>().SingleInstance();
            builder.RegisterType<AzureFileSystemRepository>().As<IFileSystemRepository>().SingleInstance();
            builder.RegisterType<KeyVaultSecretConfigurationManager>().As<ISecretConfigurationManager>().SingleInstance();
            builder.RegisterType<ServiceBusRepository>().As<IRelayRepository>().SingleInstance();
            builder.RegisterType<RedisCacheRepository>().As<ICacheRepository>().SingleInstance();
        }
    }
}
