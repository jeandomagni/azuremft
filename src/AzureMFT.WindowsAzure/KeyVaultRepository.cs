﻿using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Repositories;
using Microsoft.Azure;
using Microsoft.Azure.KeyVault;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.WindowsAzure
{
    public class KeyVaultRepository : ISecretVaultRepository
    {
        private readonly string _ADClientId;
        private readonly string _ADClientSecret;
        private readonly string _vaultName;
        private readonly bool _isProductionEnvironment;
        private readonly ILoggingService _logger;

        public KeyVaultRepository(ConfigurationManager config, ILoggingService logger) : this(logger,
            config.ADClientId,
            config.ADClientSecret,
            config.VaultName,
            config.Environment.IsProduction())
        {
            _logger = logger;
        }

        public KeyVaultRepository(ILoggingService logger, string ADClientId, string ADClientSecret, string vaultName, bool isProductionEnvironment)
        {
            _logger = logger;
            _ADClientId = ADClientId;
            _ADClientSecret = ADClientSecret;
            _vaultName = vaultName;
            _isProductionEnvironment = isProductionEnvironment;

            if (_isProductionEnvironment)
            {
                if (string.IsNullOrWhiteSpace(ADClientId) || string.IsNullOrWhiteSpace(ADClientSecret) || string.IsNullOrWhiteSpace(vaultName))
                    throw new InvalidOperationException("ADClientId, ADClientSecret and VaultName must be configured in settings in order to obtain secrets");
            }
        }

        public async Task<CertificateBundle> GetCertificateBundleAsync(string certificateKey, string certificatePasswordKey, string certificateVersion = null)
        {
            var bundle = new CertificateBundle();
            var client = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(GetToken));

            var vaultUrl = GetKeyVaultUrl();
            Secret certificate = null;
            Secret certificatePassword = null;
            if (!string.IsNullOrWhiteSpace(certificateVersion))
                certificate = await client.GetSecretAsync(vaultUrl, certificateKey, certificateVersion);
            else
                certificate = await client.GetSecretAsync(vaultUrl, certificateKey);

            certificatePassword = await client.GetSecretAsync(vaultUrl, certificatePasswordKey);
            
            bundle.Certificate = Convert.FromBase64String(certificate.Value);
            bundle.CertificatePassword = certificatePassword.Value;

            return bundle;
        }

        public Task<string> GetSecretAsync(string key, string version = null)
        {
            var kv = GetClient();

            return GetSecretAsync(key, kv, version).ContinueWith(x => !x.IsFaulted ? x.Result.Value : null);
        }

        private Task<Secret> GetSecretAsync(string key, KeyVaultClient client, string version = null)
        {
            return client.GetSecretAsync(GetKeyVaultUrl(), key, version);
        }

        public Secrets GetSecrets(bool forceKeyVault = false)
        {
            var secrets = new ConcurrentDictionary<Secrets.Names, string>();
            if ((_isProductionEnvironment) || forceKeyVault)
            {
                var kv = GetClient();
                var secretTasks = new List<Task>();
                foreach (string secretName in Enum.GetNames(typeof(Secrets.Names)))
                {
                    secretTasks.Add(GetSecretAsync(secretName, kv).ContinueWith(x =>
                    {

                        if (x.IsFaulted)
                            _logger.LogError($"Error obtaining secret definition for: {secretName} in the keyvault", x.Exception);
                        else if (string.IsNullOrEmpty(x.Result.Value))
                            _logger.LogCriticalInformation($"The value for the secret: {secretName} was null or empty");

                        secrets.TryAdd((Secrets.Names)Enum.Parse(typeof(Secrets.Names), secretName), !x.IsFaulted ? x.Result.Value : null);
                    }));
                }

                Task.WaitAll(secretTasks.ToArray());
            }
            else
            {
                // We're not in a cloud environment so we're either running local or emulated
                // Just pull whatever secrets available from the web.config/app.settings
                foreach (string secretName in Enum.GetNames(typeof(Secrets.Names)))
                {
                    secrets.TryAdd((Secrets.Names)Enum.Parse(typeof(Secrets.Names), secretName),
                        CloudConfigurationManager.GetSetting(secretName, false));
                }
            }

            return new Secrets(secrets);
        }

        private KeyVaultClient GetClient()
        {
            return new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(GetToken));
        }

        private async Task<string> GetToken(string authority, string resource, string scope)
        {
            var authContext = new AuthenticationContext(authority);
            var clientCred = new ClientCredential(_ADClientId, _ADClientSecret);
            var result = await authContext.AcquireTokenAsync(resource, clientCred);

            if (result == null)
                throw new InvalidOperationException("Failed to obtain token");

            return result.AccessToken;
        }

        private string GetKeyVaultUrl()
        {
            return $"https://{_vaultName}.vault.azure.net:443/secrets/";
        }
    }
}
