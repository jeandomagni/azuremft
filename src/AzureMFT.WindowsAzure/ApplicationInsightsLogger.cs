﻿using AzureMFT.Common;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureMFT.WindowsAzure
{
    public class ApplicationInsightsLogger : ILoggingService
    {
        private readonly TelemetryClient _telemetryClient;
        private readonly ConfigurationManager _config;

        public ApplicationInsightsLogger(ConfigurationManager config)
        {
            if (!config.Environment.IsDevelopment())
                Microsoft.ApplicationInsights.Extensibility.TelemetryConfiguration.Active.InstrumentationKey = config.ApplicationInsightsInstrumentationKey;
            else
                Microsoft.ApplicationInsights.Extensibility.TelemetryConfiguration.Active.DisableTelemetry = true;

            _telemetryClient = new TelemetryClient(Microsoft.ApplicationInsights.Extensibility.TelemetryConfiguration.Active);
            _config = config;
        }

        public void LogException(Exception ex)
        {
            LogException(ex, null, null);
        }

        public void LogException(Exception ex, string agreementName, Dictionary<string, string> properties)
        {
            var exceptionTelemetry = new ExceptionTelemetry(ex);

            if (!properties.IsNullOrEmpty())
            {
                foreach (var property in properties)
                    exceptionTelemetry.Properties.Add(property);
            }

            exceptionTelemetry.HandledAt = ExceptionHandledAt.UserCode;

            if (!string.IsNullOrWhiteSpace(agreementName))
                exceptionTelemetry.Context.User.AuthenticatedUserId = RegexLibrary.TelemetrySafeUserRegex.Replace(agreementName, "_");

            _telemetryClient.TrackException(exceptionTelemetry);
        }

        public void LogError(string message, Exception ex)
        {
            if (_config.LogLevel >= LogLevel.Error)
            {
                var exceptionTelemetry = new ExceptionTelemetry(ex);
                exceptionTelemetry.Properties["DeveloperMessage"] = message;
                exceptionTelemetry.HandledAt = ExceptionHandledAt.UserCode;
                _telemetryClient.TrackException(exceptionTelemetry);
            }
        }

        public void LogError(string message, Dictionary<string, string> properties = null)
        {
            if (_config.LogLevel >= LogLevel.Error)
            {
                if (properties != null)
                    _telemetryClient.TrackTrace(message, SeverityLevel.Error, properties);
                else
                    _telemetryClient.TrackTrace(message, SeverityLevel.Error);
            }
        }

        public void LogWarning(string message)
        {
            if (_config.LogLevel >= LogLevel.Warning)
            {
                _telemetryClient.TrackTrace(message, SeverityLevel.Warning);
            }
        }

        public void LogInformation(string message)
        {
            if (_config.LogLevel >= LogLevel.Information)
            {
                _telemetryClient.TrackTrace(message, SeverityLevel.Information);
            }
        }

        public void LogCriticalInformation(string message)
        {
            _telemetryClient.TrackTrace(message, SeverityLevel.Information);
        }

        public void LogCustom(ICustomLogEvent customEvent)
        {
            var eventTelemetry = new EventTelemetry(customEvent.EventName);

            if (!customEvent.Properties.IsNullOrEmpty())
            {
                eventTelemetry.Properties.AddRange(customEvent.Properties);
                _telemetryClient.TrackEvent(eventTelemetry);
            }
        }

        /// <summary>
        /// Logs information specifically for debug purposes
        /// </summary>
        public void LogDebug(string message)
        {
            if (_config.LogLevel >= LogLevel.Debug)
            {
                _telemetryClient.TrackTrace(message, SeverityLevel.Information);
            }
        }

        public bool HasRequiredLogLevel(LogLevel logLevel)
        {
            return _config.LogLevel >= logLevel;
        }

        public void Flush()
        {
            _telemetryClient.Flush();

            // Allow flushed log events time to be pushed.
            Thread.Sleep(1000);
        }
    }
}
