﻿using Autofac;
using AzureMFT.Server.API;
using AzureMFT.Server.API.Controllers;
using AzureMFT.Server.AS2;
using AzureMFT.Server.LB;
using AzureMFT.Server.SFTP;

namespace AzureMFT.Server
{
    public class RegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<SFTPService>().As<IMFTService>().SingleInstance();
            builder.RegisterType<AS2Service>().As<IMFTService>().SingleInstance();
            builder.RegisterType<APIService>().As<IMFTService>().SingleInstance();
            builder.RegisterType<LoadBalancerProbeService>().As<IMFTService>().SingleInstance();
            builder.RegisterType<AS2MDNFunctions>().As<IAS2MDNFunctions>().SingleInstance();
            builder.RegisterType<SFTPFileServer>().SingleInstance();
            builder.RegisterType<SFTPLogger>().SingleInstance();

            builder.RegisterType<CacheController>().InstancePerRequest();
            builder.RegisterType<LogsController>().InstancePerRequest();
        }
    }
}
