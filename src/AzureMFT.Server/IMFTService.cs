﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Server
{
    public interface IMFTService
    {
        void Start(IContainer container, Dictionary<string, IPEndPoint> endpoints);
        void Stop();
        int ShutdownOrder { get; }
    }
}
