﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using System.Net.Sockets;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Logging.Events;
using System.Threading;

namespace AzureMFT.Server.LB
{
    public class LoadBalancerProbeService : IMFTService
    {
        private readonly ILoggingService _logger;

        private Socket _listener;
        private CancellationTokenSource _cancellationTokenSource;
        private const string ServiceName = "LoadBalancerProbeService";

        public int ShutdownOrder { get { return 10; } }

        public LoadBalancerProbeService(ILoggingService logger)
        {
            _logger = logger;
        }

        public void Start(IContainer container, Dictionary<string, IPEndPoint> endpoints)
        {
            _cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = _cancellationTokenSource.Token;

            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _listener.Bind(endpoints["LB"]);
            _listener.Listen(100);

            _logger.LogCustom(new ServiceStartedEvent(ServiceName));

            Task.Factory.StartNew(() =>
            {
                try
                {
                    while (true)
                    {
                        var handler = _listener.Accept();

                        handler.Shutdown(SocketShutdown.Both);

                        handler.Close();
                    }
                }
                catch (SocketException se)
                {
                    if (se.ErrorCode != 10004)
                        throw;
                }


            }, cancellationToken);
        }

        public void Stop()
        {
            _listener.Close();
            _cancellationTokenSource.Cancel();
        }
    }
}
