﻿using AzureMFT.Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AzureMFT.Server.API.Controllers
{
    [Authorize]
    public class LogsController : ApiController
    {
        private readonly ILoggingService _logger;

        public LogsController(ILoggingService logger)
        {
            _logger = logger;
        }

        public IHttpActionResult Flush()
        {
            _logger.Flush();
            return Ok();
        }
    }
}
