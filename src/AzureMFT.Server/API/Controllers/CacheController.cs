﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Models.SFTP;
using AzureMFT.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AzureMFT.Server.API.Controllers
{
    [Authorize]
    public class CacheController : ApiController
    {
        private readonly ICacheService _cacheService;

        public CacheController()
        {

        }

        public CacheController(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        [HttpDelete]
        public IHttpActionResult Delete(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return BadRequest();

            if (_cacheService.Remove(id))
                return Ok();

            return NotFound();
        }
    }
}
