﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Microsoft.Owin.Hosting;
using Owin;
using System.Web.Http;
using Autofac.Integration.WebApi;
using AzureMFT.Common.Logging;
using AzureMFT.Common;
using AzureMFT.Common.Logging.Events;
using System.Collections;
using System.Security.Claims;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Models;
using AzureMFT.Server.API.Authentication;
using Thinktecture.IdentityModel.Owin;

namespace AzureMFT.Server.API
{
    public class APIService : IMFTService
    {
        private IDisposable _app;
        private readonly ILoggingService _logger;
        private readonly ISecretConfigurationManager _secretConfig;

        private const string ServiceName = "APIService";

        public int ShutdownOrder { get { return 2; } }

        public APIService(ILoggingService logger, ISecretConfigurationManager secretConfig)
        {
            _logger = logger;
            _secretConfig = secretConfig;
        }

        public void Start(IContainer container, Dictionary<string, IPEndPoint> endpoints)
        {
            var apiEndpoint = endpoints.ValueOrDefault("APISSL");

            if (apiEndpoint == null)
                _logger.LogException(new KeyNotFoundException("Could not find an endpoint with the name APISSL to launch the api service. API Service cannot start."));
            else
            {
                _app = WebApp.Start(new StartOptions(url: $"https://{apiEndpoint}"), (app) =>
                {

                    var apiConfig = GetAPIConfig();
                    app.UseBasicAuthentication("AzureMFTApi", Authenticate);
                    app.RequireSsl(false);

                    apiConfig.DependencyResolver = new AutofacWebApiDependencyResolver(container);
                    app.UseAutofacMiddleware(container);
                    app.UseAutofacWebApi(apiConfig);
                    app.UseWebApi(apiConfig);
                });

                _logger.LogCustom(new ServiceStartedEvent(ServiceName));
            }
        }

        private async Task<IEnumerable<Claim>> Authenticate(string apiKey)
        {
            if (apiKey == _secretConfig.GetSecret(Secrets.Names.ApiKey))
            {
                return new List<Claim>()
                {
                    new Claim("role", "trusted")
                };
            }

            return null;
        }

        public HttpConfiguration GetAPIConfig()
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional });

            return config;
        }

        public void Stop()
        {
            _app.Dispose();
        }
    }
}
