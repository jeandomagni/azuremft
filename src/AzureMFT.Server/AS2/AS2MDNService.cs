﻿using Autofac;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Logging.Events;
using AzureMFT.Common.Models;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureMFT.Server.AS2
{
    public class AS2MDNService : IMFTService
    {
        private MessageReceiver _fileReceivedReceiver;
        private MessageReceiver _mdnDispatchReceiver;
        private CancellationTokenSource _cancellationTokenSource;

        private readonly ConfigurationManager _config;
        private readonly ILoggingService _logger;

        public AS2MDNService(ConfigurationManager config, ILoggingService logger)
        {
            _config = config;
            _logger = logger;
        }

        private const string ServiceName = "AS2Service";
        public int ShutdownOrder { get { return 1; } }

        public void Start(IContainer container, Dictionary<string, IPEndPoint> endpoints)
        {
            var configManager = container.Resolve<ISecretConfigurationManager>();
            var functions = container.Resolve<IAS2MDNFunctions>();

            var factory = MessagingFactory.CreateFromConnectionString(configManager.GetSecret(Secrets.Names.RelaySBConnectionString));

            _fileReceivedReceiver = factory.CreateMessageReceiver(_config.AS2FileReceivedQueueName, ReceiveMode.PeekLock);
            _mdnDispatchReceiver = factory.CreateMessageReceiver(_config.AS2MDNDispatchQueueName, ReceiveMode.PeekLock);

            _cancellationTokenSource = new CancellationTokenSource();

            _fileReceivedReceiver.OnMessageAsync((message) => functions.OnFileReceivedAsync(message, _cancellationTokenSource.Token), new OnMessageOptions
            {
                AutoComplete = false,
                // TODO: what should these values be?
                AutoRenewTimeout = TimeSpan.FromHours(5),
                MaxConcurrentCalls = 1
            });

            _mdnDispatchReceiver.OnMessageAsync((message) => functions.OnMDNDispatchAsync(message, _cancellationTokenSource.Token), new OnMessageOptions
            {
                AutoComplete = false,
                // TODO: what should these values be?
                AutoRenewTimeout = TimeSpan.FromMinutes(1),
                MaxConcurrentCalls = 4
            });

            _logger.LogCustom(new ServiceStartedEvent(ServiceName));
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();

            // TODO: we may need to wait a little bit here to allow any currently processing messages a chance
            // to stop before aborting the receivers. not sure how that works.

            if (_fileReceivedReceiver != null)
            {
                _fileReceivedReceiver.Abort();
                _fileReceivedReceiver = null;
            }

            if (_mdnDispatchReceiver != null)
            {
                _mdnDispatchReceiver.Abort();
                _mdnDispatchReceiver = null;
            }
        }
    }
}
