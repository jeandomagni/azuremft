﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Owin;
using Microsoft.Owin.Hosting;
using Autofac;
using System.Net;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Logging.Events;
using AzureMFT.Services;

namespace AzureMFT.Server.AS2
{

    public class AS2Service : IMFTService
    {
        private List<IDisposable> _apps;

        private readonly ILoggingService _logger;

        private const string ServiceName = "AS2Service";

        public int ShutdownOrder { get { return 1; } }

        public AS2Service(ILoggingService logger)
        {
            _logger = logger;
        }

        public void Start(IContainer container, Dictionary<string, IPEndPoint> endpoints)
        {
            _apps = new List<IDisposable>();

            var nonSSL = endpoints.Select(x => x.Key).Where(x => x.StartsWith("AS2Standard"));
            foreach (var endpointName in nonSSL)
            {
                _apps.Add(WebApp.Start(new StartOptions(url: $"http://{endpoints[endpointName]}"), (app) =>
                {
                    app.UseAutofacMiddleware(container);
                    app.UseAS2ProcessorMiddleware(container.Resolve<AS2ProcessingService>());
                }));
            }

            //Doesn't seem to work. Thinks there is already another listener registered on the port.
            var ssl = endpoints.Select(x => x.Key).Where(x => x.StartsWith("AS2Secure"));
            foreach (var endpointName in ssl)
            {
                _apps.Add(WebApp.Start(new StartOptions(url: $"https://{endpoints[endpointName]}"), (app) =>
                {
                    app.UseAutofacMiddleware(container);
                    app.UseAS2ProcessorMiddleware(container.Resolve<AS2ProcessingService>());
                }));
            }

            _logger.LogCustom(new ServiceStartedEvent(ServiceName));

            if (_apps.Count == 0)
                _logger.LogException(new KeyNotFoundException("Could not find an endpoint with the names starting with AS2, or AS2SSL. AS2 Service cannot start."));
        }

        public void Stop()
        {
            foreach (var app in _apps)
            {
                app.Dispose();
            }
        }
    }
}
