﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Logging.Events;

namespace AzureMFT.Server.AS2
{
    public class LocalAS2MDNService : IMFTService
    {
        private readonly ILoggingService _logger;

        private const string ServiceName = "LocalAS2ProcessorService";

        public int ShutdownOrder { get { return 1; } }

        public LocalAS2MDNService(ILoggingService logger)
        {
            _logger = logger;
        }

        public void Start(IContainer container, Dictionary<string, IPEndPoint> endpoints)
        {
            _logger.LogCustom(new ServiceStartedEvent(ServiceName));
        }

        public void Stop()
        {
            _logger.LogCustom(new ServiceStoppedEvent(ServiceName));
        }
    }
}
