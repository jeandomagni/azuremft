﻿using AzureMFT.Common.Configuration;
using AzureMFT.Common.Exceptions;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Logging.Events;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Services;
using AzureMFT.Services;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Server.AS2
{
    public class AS2ProcessorMiddleware : OwinMiddleware
    {
        private readonly AS2ProcessingService _processor;

        public AS2ProcessorMiddleware(OwinMiddleware next, AS2ProcessingService processor)  : base(next)
        {
            _processor = processor;
        }

        public async override Task Invoke(IOwinContext context)
        {
            try
            {
                if (context.Request.Method == "POST")
                {
                    if (context.Request.Path.StartsWithSegments(new PathString("/incoming")))
                        await HandleAS2ReceiveAsync(context);
                    else
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                        await context.Response.WriteAsync($"Location not found");
                    }
                }
                else
                {
                    if (context.Request.Path == new PathString("/setup"))
                    {
                        var content = @"<!DOCTYPE html><html lang=""en"" style='font-family: Helvetica; font-size: 14px'><head><meta charset=""UTF-8""></head>"
                                    + $@"<h3>Supplylogix {_processor.AS2Config.Environment.EnvironmentName} AS2</h3>"
                                    + $@"AS2 ID: <strong>{_processor.AS2Config.AS2Id}</strong><br /><br />"
                                    + $@"Receive URL: <strong>{context.Request.Uri.Authority}/incoming</strong><br /><br />"
                                    + @"<a href=""/publiccert"">Download Public Certificate</a>"
                                    + @"</html>";

                        await context.Response.WriteAsync(content);
                    }
                    else if (context.Request.Path == new PathString("/publiccert"))
                    {
                        var dataFile = await _processor.GetPublicCertificateAsync();
                        context.Response.Headers.Append("content-disposition", $"attachment;filename={dataFile.FileName}");
                        context.Response.ContentType = dataFile.ContentType;
                        await context.Response.WriteAsync(dataFile.Data);
                    }
                    else if(context.Request.Path == new PathString("/probe"))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                        await context.Response.WriteAsync("OK");
                    }
                    else
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                        await context.Response.WriteAsync($"This system only supports POST requests.");
                    }
                }
            }
            catch (Exception ex)
            {
                _processor.Logger.LogError("An unexpected error occurred while processing the message", ex);
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
        }

        private async Task SetSynchronousContextAsync(AS2MessageDispositionNotice mdn, IOwinContext context)
        {
            context.Response.ContentType = mdn.ContentType;
            context.Response.Headers["Server"] = "Cinnabar AS2";
            context.Response.Headers["User-Agent"] = "Cinnabar AS2";
            context.Response.Headers.Append("Subject", "Message Disposition Notification");
            context.Response.Headers.Append("AS2-From", _processor.AS2Config.AS2Id);
            context.Response.Headers.Append("AS2-To", mdn.AS2To);
            context.Response.Headers.Append("AS2-Version", "1.2");
            context.Response.Headers.Append("Message-ID", $"<{mdn.MessageId}>");
            context.Response.Headers.Append("Mime-Version", "1.0");
            context.Response.Headers.Append("Content-Description", "MIME Message");
            context.Response.Headers.Append("Ediint-Features", "CEM, multiple-attachments");
            context.Response.ContentLength = mdn.Signature.Length;

            using (var stream = new MemoryStream(mdn.Signature))
            {
                await stream.CopyToAsync(context.Response.Body);
            }

            _processor.Logger.LogInformation("MDN sent synchronously");
        }

        private async Task HandleAS2ReceiveAsync(IOwinContext context)
        {
            try
            {
                var as2Message = CreateAS2MessageFromRequest(context.Request);

                if (as2Message.RequiresAsyncMDN)
                {
                    var success = await _processor.ProcessMessageAsynchronousMDNAsync(as2Message, context.Request.RemoteIpAddress);
                    if (success)
                        context.Response.StatusCode = (int)HttpStatusCode.NoContent;
                    else
                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                else
                {
                    var mdn = await _processor.ProcessMessageSyncronousMDNAsync(as2Message, context.Request.RemoteIpAddress);

                    await SetSynchronousContextAsync(mdn, context);
                }
            }
            catch (BadAS2RequestException ex)
            {
                _processor.Logger.LogError("Bad AS2 Request", ex);
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                await context.Response.WriteAsync(ex.Message);
            }
        }

        private AS2Message CreateAS2MessageFromRequest(IOwinRequest request)
        {
            var result = new AS2Message();

            IHeaderDictionary headers = request.Headers;
            result.HeaderStream = new MemoryStream();
            using (var streamWriter = new StreamWriter(result.HeaderStream, Encoding.UTF8, 4098, true))
            {
                foreach (var header in headers)
                {
                    streamWriter.WriteLine($"{header.Key}: {header.Value[0].ToString()}");
                }
                streamWriter.WriteLine();
            }
            result.HeaderStream.Seek(0, SeekOrigin.Begin);

            result.MessageId = headers.TryGetHeader(AS2HeaderKeys.MessageId);
            result.Subject = headers.TryGetHeader(AS2HeaderKeys.Subject);
            result.From = headers.TryGetHeader(AS2HeaderKeys.From);
            result.AS2Version = headers.TryGetHeader(AS2HeaderKeys.AS2Version);
            result.AS2From = headers.TryGetHeader(AS2HeaderKeys.AS2From);
            result.AS2To = headers.TryGetHeader(AS2HeaderKeys.AS2To);
            result.AS2Text = headers.TryGetHeader(AS2HeaderKeys.AS2Text);
            result.DepositionNotificationTo = headers.TryGetHeader(AS2HeaderKeys.DispositionNotificationTo);
            result.DispositionNotificationOptions = headers.TryGetHeader(AS2HeaderKeys.DispositionNotificationOptions);
            result.EDIINTFeatures = headers.TryGetHeader(AS2HeaderKeys.EDIINTFeatures);
            result.ReceiptDeliveryOption = headers.TryGetHeader(AS2HeaderKeys.ReceiptDeliveryOption);
            result.RecipientAddress = headers.TryGetHeader(AS2HeaderKeys.RecipientAddress);
            result.ContentDisposition = headers.TryGetHeader(AS2HeaderKeys.ContentDisposition);

            result.ContentType = request.ContentType;
            result.ContentObject = request.Body;

            return result;
        }
    }

    public static class HeaderExtensions
    {
        public static string TryGetHeader(this IHeaderDictionary header, string key)
        {
            if (!header.ContainsKey(key))
                return null;

            return header[key];
        }
    }

    public static class AS2ProcessorMiddlewareExtension
    {
        public static IAppBuilder UseAS2ProcessorMiddleware(this IAppBuilder app, AS2ProcessingService processor)
        {
            return app.Use<AS2ProcessorMiddleware>(processor);
        }
    }
}
