﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureMFT.Server.AS2
{
    public interface IAS2MDNFunctions
    {
        Task OnFileReceivedAsync(BrokeredMessage message, CancellationToken token);
        Task OnMDNDispatchAsync(BrokeredMessage message, CancellationToken token);
    }
}
