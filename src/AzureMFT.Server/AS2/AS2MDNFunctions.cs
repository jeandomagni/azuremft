﻿using AzureMFT.Common;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Exceptions;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Logging.Events;
using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Repositories;
using AzureMFT.Server.AS2;
using AzureMFT.Services;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureMFT.Server.AS2
{
    public class AS2MDNFunctions : IAS2MDNFunctions
    {
        private readonly ILoggingService _logger;
        private readonly IRelayRepository _relayRepository;
        private readonly ConfigurationManager _configurationManager;
        private readonly AS2ProcessingService _as2ProcessingService;
        private readonly FileSystemService _fileSystemService;
        private readonly AgreementService _agreementService;

        public AS2MDNFunctions(ILoggingService logger, IRelayRepository relayRepository, ConfigurationManager configurationManager, AS2ProcessingService as2ProcessingService, FileSystemService fileSystemService, AgreementService agreementService)
        {
            _logger = logger;
            _relayRepository = relayRepository;
            _configurationManager = configurationManager;
            _as2ProcessingService = as2ProcessingService;
            _fileSystemService = fileSystemService;
            _agreementService = agreementService;
        }

        public async Task OnFileReceivedAsync(BrokeredMessage message, CancellationToken token)
        {
            try
            {
                // TODO: handle as2 file received here.
                var as2RelayMessage = message.GetBody<RelayMessage>();

                var agreement = _agreementService.GetAgreement<AS2AgreementProperties>(as2RelayMessage.AgreementId);
                if (agreement == null)
                    _logger.LogException(new KeyNotFoundException($"Could not find an agreement with id: {as2RelayMessage.AgreementId}"));

                var streamResult = _fileSystemService.GetDownloadStream(as2RelayMessage.FilePath, agreement);
                if(streamResult.IsValid)
                {
                    var mdn = await _as2ProcessingService.ProcessIncomingMessageAsync(streamResult.Payload, agreement);

                    _logger.LogCustom(new AS2MessageProcessedAsynchronously(agreement, as2RelayMessage.FilePath, mdn.OriginalMessageId));

                    await _relayRepository.QueueAS2MDNMessageAsync(mdn);
                    //_fileSystemService.DeleteFile(as2RelayMessage.FilePath, agreement);
                    await message.CompleteAsync();
                }
                else
                {
                    _logger.LogException(new Exception($"Could not access an MDN processing blob with the path {as2RelayMessage.FilePath} and agreementId: {as2RelayMessage.AgreementId}"));
                }
            }
            catch (Exception e)
            {
                _logger.LogError("failed on file received", e);

                await message.AbandonAsync();
            }
        }

        public async Task OnMDNDispatchAsync(BrokeredMessage message, CancellationToken token)
        {
            try
            {
                var mdn = message.GetBody<AS2MessageDispositionNotice>();

                var agreement = _agreementService.GetAgreement<AS2AgreementProperties>(mdn.AS2To, AgreementType.AS2);
                if (agreement == null)
                    throw new KeyNotFoundException($"Could not send MDN because an agreement couldn't be found for AgreementName: {mdn.AS2To} and AgreementType: {AgreementType.AS2.ToString()}");

                Uri mdnSendUri;
                if (string.IsNullOrWhiteSpace(agreement.AgreementProperties.AS2Url))
                    mdnSendUri = mdn.ReceiptDeliveryOption;
                else
                    mdnSendUri = new Uri(agreement.AgreementProperties.AS2Url);
                var request = WebRequest.Create(mdnSendUri);
                request.ContentType = mdn.ContentType;
                request.Method = "POST";
                ((HttpWebRequest)request).UserAgent = "Cinnabar AS2";
                ((HttpWebRequest)request).Date = DateTime.UtcNow;
                request.Headers.Add($"Subject: Message Disposition Notification");
                request.Headers.Add($"AS2-From: {_configurationManager.AS2Id}");
                request.Headers.Add($"AS2-To: {mdn.AS2To}");
                request.Headers.Add($"AS2-Version: 1.2");
                request.Headers.Add($"Message-ID: <{mdn.MessageId}>");
                request.Headers.Add($"Mime-Version: 1.0");
                request.Headers.Add($"Content-Description: MIME Message");
                request.ContentLength = mdn.Signature.Length;

                var requestStream = request.GetRequestStream();
                using (var stream = new MemoryStream(mdn.Signature))
                {
                    stream.CopyTo(requestStream);
                }

                var mdnResponse = request.GetResponse();

                var responseStream = mdnResponse.GetResponseStream();
                var responseStatusCode = 0;
                using (var streamReader = new StreamReader(responseStream))
                {
                    var httpResponse = mdnResponse as HttpWebResponse;
                    if ((int)httpResponse.StatusCode < 200 || (int)httpResponse.StatusCode > 299)
                    {
                        var streamText = new StringBuilder();
                        streamText.AppendLine($"Unexpected status code: {(mdnResponse as HttpWebResponse).StatusCode}");
                        streamText.AppendLine($"Target: {mdn.ReceiptDeliveryOption.ToString()}");
                        streamText.AppendLine($"Body: {streamReader.ReadToEnd()}");
                        _logger.LogException(new BadAS2RequestException(streamText.ToString()));
                        responseStatusCode = (int)httpResponse.StatusCode;
                    }
                }

                _logger.LogCustom(new AcknowledgementSentEvent(mdn.OriginalMessageId, mdn.MessageId, mdn.ReceiptDeliveryOption.ToString(), responseStatusCode));

                await message.CompleteAsync();
            }
            catch (Exception e)
            {
                _logger.LogError("failed to dispatch MDN.", e);

                await message.AbandonAsync();
            }
        }
    }
}
