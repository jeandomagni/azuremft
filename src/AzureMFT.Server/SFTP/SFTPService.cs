﻿using Autofac;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Logging.Events;
using AzureMFT.Common.Models;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Services;
using AzureMFT.Services;
using Rebex;
using Rebex.Net.Servers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureMFT.Server.SFTP
{
    public class SFTPService : IMFTService
    {
        private FileServer _server;

        private readonly FileSystemService _fileSystem;
        private readonly AgreementService _agreementService;
        private readonly IRelayRepository _relayRepository;
        private readonly ConfigurationManager _config;
        private readonly ICertificateService _certificateService;
        private readonly ILoggingService _logger;
        private readonly ILogWriter _sftpLogger;
        private bool _stopping = false;

        private const string ServiceName = "SFTPService";

        public int ShutdownOrder { get { return 1; } }

        public SFTPService(SFTPFileServer server, FileSystemService fileSystem, AgreementService agreementService, IRelayRepository relayRepository, ConfigurationManager config, ICertificateService certificateService, ILoggingService logger, SFTPLogger sftpLogger)
        {
            _server = server;
            _fileSystem = fileSystem;
            _agreementService = agreementService;
            _relayRepository = relayRepository;
            _config = config;
            _certificateService = certificateService;
            _logger = logger;
            _sftpLogger = sftpLogger;
        }

        public void Start(IContainer container, Dictionary<string, IPEndPoint> endpoints)
        {
            if (!endpoints.ContainsKey("SFTP"))
                _logger.LogException(new KeyNotFoundException("Could not find an endpoint with the name SFTP. SFTP Service cannot start."));
            else
            {
                _server.Bind(endpoints["SFTP"], FileServerProtocol.Sftp);

                var bundle = _certificateService.GetCertificateBundleAsync(_config.SFTPHostCertificateKey, _config.SFTPHostCertificatePasswordKey, _config.SFTPHostCertificateVersion).GetAwaiter().GetResult();
                var x509 = new X509Certificate2(bundle.Certificate, bundle.CertificatePassword);

                _server.Settings.AllowedAuthenticationMethods = AuthenticationMethods.PublicKey | AuthenticationMethods.Password;
                _server.Keys.Add(new Rebex.Net.SshPrivateKey(x509.PrivateKey));
                _server.Authentication += _server_Authentication;
                _server.ErrorOccurred += _server_ErrorOccurred;
                _server.Connecting += _server_Connecting;
                _server.FileUploaded += _server_FileUploaded;
                _server.FileDownloaded += _server_FileDownloaded;
                _server.LogWriter = _sftpLogger;

                _server.Start();
                _logger.LogCustom(new ServiceStartedEvent(ServiceName));
            }
        }

        private void _server_FileDownloaded(object sender, FileTransferredEventArgs e)
        {
            var fileServerUser = e.User as SFTPFileServerUser;

            _logger.LogCustom(new FileSentEvent(fileServerUser.Agreement, e.Path, e.BytesTransferred));
        }

        private void _server_FileUploaded(object sender, FileTransferredEventArgs e)
        {
            var fileServerUser = e.User as SFTPFileServerUser;

            var relayMessage = new RelayMessage(fileServerUser.Agreement.AgreementId, fileServerUser.Agreement.TradingPartner.TradingPartnerId, e.Path, DateTime.UtcNow);

            _logger.LogCustom(new FileReceivedEvent(fileServerUser.Agreement, e.Path, e.BytesTransferred));

            _relayRepository.QueueRelayMessageAsync(relayMessage);
        }

        private void _server_Connecting(object sender, ConnectingEventArgs e)
        {
            if (!_stopping)
                e.Accept = _agreementService.ShouldAcceptConnection(e.ClientAddress.ToString()).IsValid;
            else
                e.Accept = false;
        }

        private void _server_ErrorOccurred(object sender, Rebex.Net.Servers.ErrorEventArgs e)
        {
            if(e.Error.Message != "Broken packet - bad length.")
                _logger.LogException(e.Error);
        }

        private void _server_Authentication(object sender, AuthenticationEventArgs e)
        {
            try
            {
                var clientIP = e.ClientAddress.ToString();

                byte[] publicKey = null;
                if (e.Key != null)
                    publicKey = e.Key.GetPublicKey();
                var result = _agreementService.Authenticate(e.UserName, publicKey, e.Password, clientIP);

                if (result.IsValid)
                {
                    var fileServerUser = new SFTPFileServerUser(result.Payload);

                    e.Accept(fileServerUser);
                    return;
                }

                e.Reject();
            }
            catch(System.Security.Cryptography.CryptographicException ex)
            {
                _logger.LogError("Caught crypto exception", ex);
            }
        }

        public void Stop()
        {
            if (_server != null)
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                // Attempt to wait as long as possible before shutting down if there are still connections open.
                while(_server.IsRunning && _server.Sessions.Any(x => x.State == SessionState.Running) && stopwatch.ElapsedMilliseconds < 25000)
                {
                    _stopping = true;
                    Thread.Sleep(2000);
                }
                _server.Stop();
                _server.Unbind();
            }
        }
    }
}
