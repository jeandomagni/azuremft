﻿using AzureMFT.Common.Logging;
using Rebex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Server.SFTP
{
    public class SFTPLogger : ILogWriter
    {
        private readonly ILoggingService _logger;

        public Rebex.LogLevel Level { get; set; }

        public SFTPLogger(ILoggingService logger)
        {
            Level = Rebex.LogLevel.Error;
            _logger = logger;
        }

        public void Write(Rebex.LogLevel level, Type objectType, int objectId, string area, string message)
        {
            _logger.LogError(message);
        }

        public void Write(Rebex.LogLevel level, Type objectType, int objectId, string area, string message, byte[] buffer, int offset, int length)
        {
            _logger.LogError(message);
        }
    }
}
