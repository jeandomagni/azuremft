﻿using AzureMFT.Common.Configuration;
using AzureMFT.Common.Models;
using AzureMFT.Services;
using Rebex.IO;
using Rebex.Net.Servers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Server.SFTP
{
    public class SFTPFileServer : FileServer
    {
        private readonly FileSystemService _fileSystemService;

        public SFTPFileServer(FileSystemService filesSystemService, ISecretConfigurationManager secretConfig, ConfigurationManager config)
        {
            _fileSystemService = filesSystemService;
        }

        protected override IFilesystem GetFilesystem(ServerUser user)
        {
            return new SFTPFileSystem(_fileSystemService, user as SFTPFileServerUser);
        }
    }
}
