﻿using AzureMFT.Common.Models;
using Rebex.IO;
using Rebex.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureMFT.Common.Result;
using AzureMFT.Common.Validation;
using AzureMFT.Services;
using AzureMFT.Common.Models.SFTP;

namespace AzureMFT.Server.SFTP
{
    public class SFTPFileSystem : IFilesystem
    {
        private readonly FileSystemService _fileSystem;
        private readonly Agreement<SFTPAgreementProperties> _agreement;
        private readonly FilesystemInfo _fileSystemInfo;

        public SFTPFileSystem(FileSystemService fileSystemService, SFTPFileServerUser user)
        {
            _fileSystem = fileSystemService;
            _agreement = user.Agreement;
            _fileSystemInfo = new FilesystemInfo('/', '\\', '/', false, new char[] { '?', '&' });
        }
        
        public void Close(IHandle handle)
        {
            try
            {
                if (handle == null)
                    throw new ArgumentNullException("handle");

                var fh = handle as FileHandle;
                var dh = handle as DirectoryHandle;

                if (fh == null && dh == null)
                    throw new FilesystemException(FilesystemExceptionStatus.InvalidHandle, "Invalid handle.");

                if (fh != null)
                    fh.Dispose();

                if (dh != null)
                    dh.Dispose();
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }

            if (handle is FileHandle)
                (handle as FileHandle).Dispose();
            else if (handle is DirectoryHandle)
                (handle as DirectoryHandle).Dispose();
        }

        public void CreateDirectory(string path)
        {
            throw new FilesystemException(FilesystemExceptionStatus.Unspecified, "Creating directories is not supported");
        }

        public void DeleteDirectory(string path)
        {
            throw new FilesystemException(FilesystemExceptionStatus.Unspecified, "Deleting directories is not supported");
        }

        public void DeleteFile(string path)
        {
            try
            {
                var result = _fileSystem.DeleteFile(path, _agreement);

                HandleErrors(result);
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }
        }

        public FilesystemInfo GetFilesystemInfo()
        {
            return _fileSystemInfo;
        }

        public ItemInfo GetItemInfo(string path)
        {
            try
            {
                var pathListingItem = _fileSystem.GetFileAttributes(path, _agreement);

                if (pathListingItem == null)
                    throw new FilesystemException(FilesystemExceptionStatus.PathNotFound, "Path not found.");

                return ConvertToItemInfo(pathListingItem);
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }
        }

        public string Normalize(string absolutePath)
        {
            throw new FilesystemException(FilesystemExceptionStatus.Unspecified, "Unsupported action.");
        }

        public IHandle OpenDirectory(string path, string mask)
        {
            if (path == null)
                throw new ArgumentNullException("path");

            if (mask == null || mask.Length == 0)
                mask = "*";

            path = path.Replace(Path.GetTempPath(), string.Empty);
            if (!string.IsNullOrEmpty(path))
                path += "/";

            DirectoryHandle handle = null;
            try
            {
                handle = new DirectoryHandle(_fileSystem.GetPathListing(path, _agreement));
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }

            return handle;
        }

        public IHandle OpenFile(string path, FileOpenMode openMode, FileAccessMode accessMode)
        {
            try
            {
                IHandle handle = null;
                GenericResult<Stream> uploadStreamResult = null;
                GenericResult<Stream> downloadStreamResult = null;
                switch (openMode)
                {
                    case FileOpenMode.CreateNew: case FileOpenMode.CreateOrTruncate: case FileOpenMode.OpenOrCreate: 
                        uploadStreamResult = _fileSystem.GetUploadStream(path, _agreement);

                        HandleErrors(uploadStreamResult);

                        handle = new FileHandle(Path.GetFileName(path), uploadStreamResult.Payload);
                        break;
                    case FileOpenMode.OpenExisting:
                        downloadStreamResult = _fileSystem.GetDownloadStream(path, _agreement);

                        HandleErrors(downloadStreamResult);

                        handle = new FileHandle(Path.GetFileName(path), downloadStreamResult.Payload);
                        break;
                    case FileOpenMode.Truncate:
                        throw new FilesystemException(FilesystemExceptionStatus.Unspecified, "Unsupported action.");
                }

                return handle;
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }
        }

        public int Read(IHandle handle, byte[] buffer, int offset, int count)
        {
            try
            {
                var fileHandle = handle as FileHandle;

                return fileHandle.Read(buffer, offset, count);
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }
        }

        public ItemInfo ReadNextItem(IHandle handle)
        {
            try
            {
                var nextHandle = handle as DirectoryHandle;

                return nextHandle.ReadNext();
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }
        }

        public void Rename(string sourcePath, string targetPath)
        {
            throw new FilesystemException(FilesystemExceptionStatus.Unspecified, "Unsupported action.");

            //try
            //{
            //    var result = _fileSystem.RenameFile(sourcePath, targetPath, _agreement);

            //    HandleErrors(result);
            //}
            //catch(Exception ex)
            //{
            //    throw ProcessError(ex);
            //}
        }

        public string ResolvePath(string path, string relativePath)
        {
            return relativePath;
        }

        public long Seek(IHandle handle, FileSeekOrigin origin, long offset)
        {
            try
            {
                var fileHandle = handle as FileHandle;

                return fileHandle.Seek(origin, offset);
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }
        }

        public void SetItemInfo(string path, ItemInfo info)
        {
            // Handled automatically by the file system
        }

        public void SetItemInfo(IHandle handle, ItemInfo info)
        {
            // Handled automatically by the file system
        }

        public void Write(IHandle handle, byte[] buffer, int offset, int count)
        {
            try
            {
                var fileHandle = handle as FileHandle;

                fileHandle.Write(buffer, offset, count);
            }
            catch(Exception ex)
            {
                throw ProcessError(ex);
            }
            
        }

        public class DirectoryHandle : IHandle
        {
            private IEnumerator<ItemInfo> _listing;
            private bool _available;

            public DirectoryHandle(IEnumerable<PathListingItem> listing)
            {
                _listing = ConvertToItemInfoList(listing).GetEnumerator();
                _available = _listing.MoveNext();
            }

            public ItemInfo ReadNext()
            {
                if (_listing == null)
                    throw new ObjectDisposedException(GetType().FullName);

                if(_available)
                {
                    var value = _listing.Current;
                    _available = _listing.MoveNext();
                    return value;
                }
                else
                {
                    return null;
                }
            }

            public void Dispose()
            {
                if (_listing != null)
                {
                    _listing.Dispose();
                    _listing = null;
                }
            }
        }

        private class FileHandle : IHandle
        {
            private Stream _stream { get; set; }
            public string FileName { get; private set; }

            public FileHandle(string fileName, Stream stream)
            {
                FileName = fileName;
                _stream = stream;
            }

            public void Dispose()
            {
                if(_stream != null)
                {
                    _stream.Flush();
                    _stream.Close();
                    _stream = null;
                }
            }

            internal int Read(byte[] buffer, int offset, int count)
            {
                CheckNotDisposed();
                return _stream.Read(buffer, offset, count);
            }

            internal void Write(byte[] buffer, int offset, int count)
            {
                CheckNotDisposed();
                _stream.Write(buffer, offset, count);
            }

            internal long Seek(FileSeekOrigin origin, long offset)
            {
                CheckNotDisposed();
                return _stream.Seek(Math.Min(_stream.Length, offset), ToSeekOrigin(origin));
            }

            internal void SetLength(long length)
            {
                CheckNotDisposed();
                _stream.SetLength(length);
            }

            private SeekOrigin ToSeekOrigin(FileSeekOrigin origin)
            {
                switch (origin)
                {
                    case FileSeekOrigin.Begin:
                        return SeekOrigin.Begin;
                    case FileSeekOrigin.Current:
                        return SeekOrigin.Current;
                    case FileSeekOrigin.End:
                        return SeekOrigin.End;
                    default:
                        throw new InvalidOperationException($"Unknown FileSeekOrigin: {origin.ToString()}");
                }
            }

            private void CheckNotDisposed()
            {
                if (_stream == null)
                    throw new ObjectDisposedException(GetType().FullName);
            }
        }

        public static IEnumerable<ItemInfo> ConvertToItemInfoList(IEnumerable<PathListingItem> listing)
        {
            var result = new List<ItemInfo>();
            foreach (var item in listing)
            {
                result.Add(ConvertToItemInfo(item));
            }

            return result;
        }

        public static ItemInfo ConvertToItemInfo(PathListingItem item)
        {
            ItemType? itemType = null;
            switch (item.ItemType)
            {
                case PathListingItemType.File:
                    itemType = ItemType.File;
                    break;
                case PathListingItemType.Directory:
                    itemType = ItemType.Directory;
                    break;
            }


            return new ItemInfo(itemType, null, item.Name, item.Size, null, item.LastModifiedUtcDate, null);
        }

        private static Exception ProcessError(Exception error)
        {
            var ae = error as ArgumentException;
            var fse = error as FilesystemException;
            if (ae != null)
            {
                if (ae.ParamName == "path")
                    return new FilesystemException(FilesystemExceptionStatus.InvalidPath, "Invalid path.");

                throw new FilesystemException(FilesystemExceptionStatus.Unspecified, 0, "Invalid argument.", error);
            }

            if (fse != null)
                return fse;

            return new FilesystemException(FilesystemExceptionStatus.Unspecified, 0, "I/O error.", error);
        }

        private void HandleErrors(ValidationResult result)
        {
            if (!result.IsValid)
                throw new FilesystemException(FilesystemExceptionStatus.Unspecified, result.RuleViolations.First().ErrorMessage);
        }
    }
}
