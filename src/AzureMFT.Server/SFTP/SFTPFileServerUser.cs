﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Models.SFTP;
using Rebex.Net.Servers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Server.SFTP
{
    public class SFTPFileServerUser : FileServerUser
    {
        public readonly Agreement<SFTPAgreementProperties> Agreement;

        public SFTPFileServerUser(Agreement<SFTPAgreementProperties> agreement) : base(agreement.AgreementName, null, Path.GetTempPath(), "")
        {
            this.Agreement = agreement;
        }
    }
}
