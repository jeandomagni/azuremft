﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Exceptions
{
    public class BadAS2RequestException : Exception
    {
        public BadAS2RequestException(string message) : base(message)
        {

        }
    }
}
