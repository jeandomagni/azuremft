﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models
{
    [DataContract]
    public class CertificateBundle
    {
        [DataMember(Order = 1)]
        public byte[] Certificate { get; set; }

        [DataMember(Order = 2)]
        public string CertificatePassword { get; set; }
    }
}
