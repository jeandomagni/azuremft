﻿using AzureMFT.Common.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models.AS2
{
    public class AS2Config
    {
        public HostingEnvironment Environment { get; set; }
        public string AS2Id { get; set; }
    }
}
