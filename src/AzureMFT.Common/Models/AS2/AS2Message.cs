﻿using AzureMFT.Common.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models.AS2
{
    public class AS2Message
    {
        public string MessageId { get; set; }
        public string Subject { get; set; }
        public string From { get; set; }
        public string AS2Version { get; set; }
        public string AS2From { get; set; }
        public string AS2To { get; set; }
        public string AS2Text { get; set; }
        public string DepositionNotificationTo { get; set; }
        public string DispositionNotificationOptions { get; set; }
        public string EDIINTFeatures { get; set; }
        public string ReceiptDeliveryOption { get; set; }
        public string RecipientAddress { get; set; }
        public string ContentDisposition { get; set; }
        public string ContentType { get; set; }
        public Stream ContentObject { get; set; }
        public Stream HeaderStream { get; set; }

        public bool RequiresAsyncMDN
        {
            get
            {
                return !string.IsNullOrWhiteSpace(ReceiptDeliveryOption);
            }
        }

        public override string ToString()
        {
            return $"Message for: {this.AS2To} with subject: {this.Subject} and id: {this.MessageId} was received from: {this.AS2From}";
        }

        public string GetMICAlg()
        {
            string micalg = null;
            if (!string.IsNullOrWhiteSpace(DispositionNotificationOptions))
            {
                var match = RegexLibrary.MICAlgRegex.Match(DispositionNotificationOptions.RemoveWhitespace());
                if (match.Success && match.Groups.Count > 1 && match.Groups[1].Success)
                {
                    micalg = match.Groups[1].Value;
                }
            }
            return micalg;
        }
    }
}
