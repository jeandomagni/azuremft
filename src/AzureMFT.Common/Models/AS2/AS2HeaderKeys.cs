﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models.AS2
{
    public static class AS2HeaderKeys
    {
        public static readonly string MessageId = "message-id";
        public static readonly string Subject = "subject";
        public static readonly string From = "from";
        public static readonly string AS2Version = "as2-version";
        public static readonly string AS2From = "as2-from";
        public static readonly string AS2To = "as2-to";
        public static readonly string AS2Text = "as2-text";
        public static readonly string DispositionNotificationTo = "disposition-notification-to";
        public static readonly string DispositionNotificationOptions = "disposition-notification-options";
        public static readonly string EDIINTFeatures = "ediint-features";
        public static readonly string ReceiptDeliveryOption = "receipt-delivery-option";
        public static readonly string RecipientAddress = "recipient-address";
        public static readonly string ContentDisposition = "content-disposition";
    }
}
