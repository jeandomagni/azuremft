﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models.AS2
{
    [DataContract]
    public class AS2MessageDispositionNotice
    {
        [DataMember]
        public byte[] Signature { get; set; }

        [DataMember]
        public Uri ReceiptDeliveryOption { get; set; }

        [DataMember]
        public string AS2To { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public string MessageId { get; set; }

        [DataMember]
        public string OriginalMessageId { get; set; }

        public AS2MessageDispositionNotice(string messageId, string originalMessageId)
        {
            MessageId = messageId;
            OriginalMessageId = originalMessageId;
        }
    }
}
