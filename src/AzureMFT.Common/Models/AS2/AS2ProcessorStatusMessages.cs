﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models.AS2
{
    public static class AS2ProcessorStatusMessages
    {
        public static readonly string UnsupportedMICAlgorithm = "failed/failure: unsupported MIC-algorithms";
        public static readonly string UnsupportedFormat = "failed/failure: unsupported format";
        public static readonly string SenderEqualsReceiver = "failed/failure: sender-equals-receiver";

        public static readonly string DecryptionFailed = "processed/error: decryption-failed";
        public static readonly string DecompressionFailed = "processed/error: decompression-failed";
        public static readonly string AuthenticationFailed = "processed/error: authentication-failed";
        public static readonly string IntegrityCheckFailed = "processed/error: integrity-check-failed";
        public static readonly string UnexpectedProcessingError = "processed/error: unexpected-processing-error";

        public static readonly string DuplicateDocument = "processed/warning: duplicate-document";

        public static readonly string SuccessfullyProcessed = "processed";
    }
}
