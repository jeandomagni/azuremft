﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models.AS2
{
    [DataContract]
    public class AS2AgreementProperties
    {
        [DataMember(Order = 1)]
        public byte[] PartnerCertificate { get; set; }

        [DataMember(Order = 2)]
        public byte[] PartnerCertificateSecondary { get; set; }

        [DataMember(Order = 3)]
        public string RootCertificateKey { get; set; }

        [DataMember(Order = 4)]
        public string RootCertificateVersion { get; set; }

        [DataMember(Order = 5)]
        public string RootCertificatePasswordKey { get; set; }

        [DataMember(Order = 6)]
        public string AS2Url { get; set; }
    }
}
