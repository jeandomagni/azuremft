﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models.SFTP
{
    [DataContract]
    public class SFTPAgreementProperties
    {
        [DataMember(Order = 1)]
        public byte[] PartnerSSHKey { get; set; }

        [DataMember(Order = 2)]
        public byte[] PartnerSSHKeySecondary { get; set; }

        [DataMember(Order = 3)]
        public string Password { get; set; }

        [DataMember(Order = 4)]
        public string PasswordSalt { get; set; }
    }
}
