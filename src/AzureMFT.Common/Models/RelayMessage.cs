﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models
{
    [DataContract]
    public class RelayMessage
    {
        [DataMember]
        public int AgreementId { get; private set; }

        [DataMember]
        public int TradingPartnerId { get; set; }

        [DataMember]
        public string FilePath { get; set; }

        [DataMember]
        public DateTime DataReceived { get; private set; }

        public RelayMessage(int agreementId, int tradingPartnerId, string filePath, DateTime dataReceived)
        {
            AgreementId = agreementId;
            TradingPartnerId = tradingPartnerId;
            FilePath = filePath;
            DataReceived = dataReceived;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Relay Message:");
            sb.AppendLine($"AgreementId: {AgreementId}");
            sb.AppendLine($"TradingPartnerId: {TradingPartnerId}");
            sb.AppendLine($"FilePath: {FilePath}");
            sb.AppendLine($"DataReceived: {DataReceived}");
            return sb.ToString();
        }
    }
}
