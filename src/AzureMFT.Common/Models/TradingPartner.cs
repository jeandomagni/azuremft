﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models
{
    [DataContract]
    public class TradingPartner
    {
        [DataMember(Order = 1)]
        public int TradingPartnerId { get; set; }

        [DataMember(Order = 2)]
        public string TradingPartnerName { get; set; }
    }
}
