﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models
{
    public enum PathListingItemType
    {
        File = 0,
        Directory
    }
}
