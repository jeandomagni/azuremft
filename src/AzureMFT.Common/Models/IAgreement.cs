﻿using AzureMFT.Common.Models.AS2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models
{
    public interface IAgreement
    {
        int AgreementId { get; set; }
        string AgreementName { get; set; }
        AgreementType AgreementType { get; set; }
        TradingPartner TradingPartner { get; set; }
        DateTime CreatedUtcDate { get; set; }
        int CreatedByUserId { get; set; }
        DateTime? LastUsedUtcDate { get; set; }
        int FailedAuthenticationAttemptCount { get; set; }
        bool LockedOut { get; set; }
        DateTime? LastLockedOutUtcDate { get; set; }

        Dictionary<string, string> GetPropertiesFormat();
    }
}
