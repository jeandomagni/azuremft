﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AzureMFT.Common.Models.Secrets;

namespace AzureMFT.Common.Models
{
    public class Secrets : ReadOnlyDictionary<Names, string>
    {
        public Secrets(IDictionary<Names, string> dictionary) : base(dictionary)
        {

        }

        public enum Names
        {
            StorageConnectionString,
            SqlConnectionString,
            RelaySBConnectionString,
            RedisConfiguration,
            ApiKey
        }

        public string Get(Names key)
        {
            return this.ValueOrDefault(key);
        }
    }
}
