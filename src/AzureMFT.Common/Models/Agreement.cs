﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models
{
    [DataContract]
    public class Agreement<T> : IAgreement
    {
        [DataMember(Order = 1)]
        public int AgreementId { get; set; }

        [DataMember(Order = 2)]
        public string AgreementName { get; set; }

        [DataMember(Order = 3)]
        public AgreementType AgreementType { get; set; }

        [DataMember(Order = 4)]
        public TradingPartner TradingPartner { get; set; }

        [DataMember(Order = 5)]
        public DateTime CreatedUtcDate { get; set; }

        [DataMember(Order = 6)]
        public int CreatedByUserId { get; set; }

        [DataMember(Order = 7)]
        public DateTime? LastUsedUtcDate { get; set; }

        [DataMember(Order = 8)]
        public int FailedAuthenticationAttemptCount { get; set; }

        [DataMember(Order = 9)]
        public bool LockedOut { get; set; }

        [DataMember(Order = 10)]
        public DateTime? LastLockedOutUtcDate { get; set; }

        [DataMember(Order = 11)]
        public T AgreementProperties { get; set; }

        public Dictionary<string, string> GetPropertiesFormat()
        {
            return new Dictionary<string, string>()
            {
                { "TradingPartnerName", TradingPartner.TradingPartnerName },
                { "TradingPartnerId", TradingPartner.TradingPartnerId.ToString() },
                { "AgreementName", AgreementName },
                { "AgreementId", AgreementId.ToString() }
            };
        }
    }
}
