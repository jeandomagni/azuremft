﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Models
{
    public class PathListingItem
    {
        public string Name { get; set; }
        public DateTime? LastModifiedUtcDate { get; set; }
        public PathListingItemType ItemType { get; set; }
        public long Size { get; set; }
    }
}
