﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common
{
    public static class DIRegistrationHelper
    {
        public static Assembly[] GetRegistrationAssemblies()
        {
            var assemblies = new List<Assembly>();
            var directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            foreach (var file in System.IO.Directory.GetFiles(directory, "AzureMFT*.dll"))
            {
                assemblies.Add(Assembly.LoadFile(file));
            }

            return assemblies.ToArray();
        }
    }
}
