﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Validation
{
    [Serializable]
    public class CodedValidationResultMessage<T> : ValidationResultMessage
    {
        public T Code { get; set; }

        public CodedValidationResultMessage()
        {

        }

        public CodedValidationResultMessage(T code, string message, ValidationResultMessageType type) : base(message, type)
        {
            Code = code;
        }
    }
}
