﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Validation
{
    public static class Validation
    {
        public static Validation<TValidationResult> Validate<TValidationResult>() where TValidationResult : ValidationResult
        {
            return new Validation<TValidationResult>();
        }

        public static Validation<TValidationResult> Validate<TValidationResult>(Func<bool> condition, string parameterName, string errorMessage) where TValidationResult : ValidationResult
        {
            return new Validation<TValidationResult>(condition, parameterName, errorMessage);
        }
    }

    public class Validation<TValidationResult> where TValidationResult : ValidationResult
    {
        public Validation()
        {

        }

        public Validation(Func<bool> condition, string parameterName, string errorMessage)
        {
            Require(condition, parameterName, errorMessage);
        }

        public Validation<TValidationResult> Require(Func<bool> condition, string parameterName, string errorMessage)
        {
            Condition = condition;
            ParameterName = parameterName;
            ErrorMessage = errorMessage;

            return this;
        }

        protected Func<bool> Condition
        {
            get;
            private set;
        }

        protected string ParameterName
        {
            get;
            private set;
        }

        protected string ErrorMessage
        {
            get;
            private set;
        }

        public Validation<TValidationResult> Or(Func<bool> condition, string parameterName, string errorMessage)
        {
            return new OrValidation(this, condition, parameterName, errorMessage);
        }

        public Validation<TValidationResult> And(Func<bool> condition, string parameterName, string errorMessage)
        {
            return new AndValidation(this, condition, parameterName, errorMessage);
        }

        public TValidationResult Result()
        {
            IList<RuleViolation> ruleViolations = Validate();
            IList<ValidationResultMessage> messages = new List<ValidationResultMessage>();

            return (TValidationResult)Activator.CreateInstance(typeof(TValidationResult), new object[] { ruleViolations, messages });
        }

        protected virtual IList<RuleViolation> Validate()
        {
            IList<RuleViolation> violations = new List<RuleViolation>();

            if (Condition())
            {
                violations.Add(new RuleViolation(ParameterName, ErrorMessage));
            }

            return violations;
        }

        private abstract class CompositeValidation : Validation<TValidationResult>
        {
            protected CompositeValidation(Validation<TValidationResult> innerValidation, Func<bool> condition, string parameterName, string errorMessage)
                : base(condition, parameterName, errorMessage)
            {
                InnerValidation = innerValidation;
            }

            protected Validation<TValidationResult> InnerValidation
            {
                get;
                private set;
            }
        }

        private sealed class OrValidation : CompositeValidation
        {
            public OrValidation(Validation<TValidationResult> innerValidation, Func<bool> condition, string parameterName, string errorMessage)
                : base(innerValidation, condition, parameterName, errorMessage)
            {
            }

            protected override IList<RuleViolation> Validate()
            {
                IList<RuleViolation> violations = InnerValidation.Validate();

                if (violations.IsEmpty())
                {
                    violations.AddRange(base.Validate());
                }

                return violations;
            }
        }

        private sealed class AndValidation : CompositeValidation
        {
            public AndValidation(Validation<TValidationResult> innerValidation, Func<bool> condition, string parameterName, string errorMessage)
                : base(innerValidation, condition, parameterName, errorMessage)
            {
            }

            protected override IList<RuleViolation> Validate()
            {
                IList<RuleViolation> violations = InnerValidation.Validate();

                violations.AddRange(base.Validate());

                return violations;
            }
        }
    }
}
