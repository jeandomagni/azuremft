﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Validation
{
    public class ValidationResult
    {
        public ValidationResult() : this(new List<RuleViolation>(), new List<ValidationResultMessage>()) { }

        public ValidationResult(IEnumerable<RuleViolation> ruleViolations, IEnumerable<ValidationResultMessage> messages)
        {
            RuleViolations = new List<RuleViolation>(ruleViolations);
            Messages = new List<ValidationResultMessage>(messages);
        }

        public IList<RuleViolation> RuleViolations
        {
            get;
            private set;
        }

        public IList<ValidationResultMessage> Messages
        {
            get;
            private set;
        }

        public bool IsValid
        {
            get
            {
                return this.RuleViolations.Count == 0 && !this.Messages.Any(x => x.Type == ValidationResultMessageType.error);
            }
        }

        public ValidationResult AddViolation(string name, string message)
        {
            RuleViolations.Add(new RuleViolation(name, message));
            return this;
        }

        public ValidationResult AddMessage(string message, ValidationResultMessageType messageType)
        {
            Messages.Add(new ValidationResultMessage()
            {
                Message = message,
                Type = messageType
            });

            return this;
        }
    }
}
