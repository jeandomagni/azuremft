﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Validation
{
    [Serializable]
    public class ValidationResultMessage
    {
        public ValidationResultMessageType Type { get; set; }
        public string Message { get; set; }

        public ValidationResultMessage()
        {

        }

        public ValidationResultMessage(string message, ValidationResultMessageType type)
        {
            Message = message;
            Type = type;
        }
    }
}
