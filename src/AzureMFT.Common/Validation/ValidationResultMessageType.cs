﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Validation
{
    public enum ValidationResultMessageType
    {
        success = 1,
        warning,
        error,
        information
    }
}
