﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Result
{
    public class FileSystemResult : ValidationResult
    {
        public FileSystemResult() : this(new List<RuleViolation>(), new List<CodedValidationResultMessage<FileSystemCodes>>()) { }
        public FileSystemResult(IEnumerable<RuleViolation> ruleViolations, IEnumerable<CodedValidationResultMessage<FileSystemCodes>> messages) : base(ruleViolations, messages)
        {
        }
    }
}
