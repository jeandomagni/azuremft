﻿using AzureMFT.Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Result
{
    public class GenericResult<T> : ValidationResult
    {
        public T Payload { get; set; }

        public GenericResult() : this(new List<RuleViolation>(), new List<ValidationResultMessage>()) { }
        public GenericResult(ValidationResult validationResult) : base(validationResult.RuleViolations, validationResult.Messages) { }
        public GenericResult(IEnumerable<RuleViolation> ruleViolations, IEnumerable<ValidationResultMessage> messages) : base(ruleViolations, messages) { }
        

    }
}
