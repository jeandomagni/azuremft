﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Result
{
    public class ProcessAS2MessageResult
    {
        public string DispositionMessage { get; set; }
        public string MessageIntegrityCheck { get; set; }
        public string MICAlg { get; set; }
    }
}
