﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureMFT.Common.Validation;
using System.IO;

namespace AzureMFT.Common.Result
{
    public class GetUploadStreamResult : GenericResult<FileSystemCodes>
    {
        public Stream UploadStream { get; set; }

        public GetUploadStreamResult() : base(new List<RuleViolation>(), new List<ValidationResultMessage>()) { }
        public GetUploadStreamResult(IEnumerable<RuleViolation> ruleViolations, IEnumerable<ValidationResultMessage> messages) : base(ruleViolations, messages) { }
    }
}
