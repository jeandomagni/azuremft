﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AzureMFT.Common
{
    public static class RegexLibrary
    {
        public const string TelemetrySafeUser = @"[,;=| ]+";

        private static Regex _telemetrySafeUserRegex;
        public static Regex TelemetrySafeUserRegex
        {
            get
            {
                if (_telemetrySafeUserRegex == null)
                {
                    _telemetrySafeUserRegex = new Regex(TelemetrySafeUser, RegexOptions.Compiled);
                }
                return _telemetrySafeUserRegex;
            }
        }

        public const string Whitespace = @"\s+";

        private static Regex _whitespaceRegex;
        public static Regex WhitespaceRegex
        {
            get
            {
                if (_whitespaceRegex == null)
                {
                    _whitespaceRegex = new Regex(Whitespace, RegexOptions.Compiled);
                }
                return _whitespaceRegex;
            }
        }

        public const string MICAlg = @"signed-receipt-micalg=optional,(.+)";

        private static Regex _micalg;
        public static Regex MICAlgRegex
        {
            get
            {
                if (_micalg == null)
                {
                    _micalg = new Regex(MICAlg, RegexOptions.Compiled);
                }
                return _micalg;
            }
        }
    }
}
