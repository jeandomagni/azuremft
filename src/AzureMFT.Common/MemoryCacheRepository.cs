﻿using AzureMFT.Common.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureMFT.Common.Cache;
using System.Runtime.Caching;

namespace AzureMFT.Common
{
    public class MemoryCacheRepository : ICacheRepository
    {
        private MemoryCache _cache;

        public MemoryCacheRepository()
        {
            _cache = new MemoryCache("cache");
        }

        public byte[] Get(string key)
        {
            return _cache.Get(key) as byte[];
        }

        public bool Remove(string key)
        {
            var removed = _cache.Remove(key);

            return removed != null;
        }

        public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
        {
            var policy = new CacheItemPolicy();
            if (options.AbsoluteExpiration.HasValue)
                policy.AbsoluteExpiration = options.AbsoluteExpiration.Value;
            if (options.SlidingExpiration.HasValue)
                policy.SlidingExpiration = options.SlidingExpiration.Value;

            _cache.Set(key, value, policy);
        }
    }
}
