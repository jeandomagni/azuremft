﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Repositories
{
    public interface IBanListRepository
    {
        HashSet<string> GetBanList();
        void AddToBanList(string ipAddress);
    }
}
