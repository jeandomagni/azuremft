﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Repositories
{
    public interface ISecretVaultRepository
    {
        Task<CertificateBundle> GetCertificateBundleAsync(string certificateKey, string certificatePasswordKey, string certificateVersion = null);

        Task<string> GetSecretAsync(string key, string version = null);

        Secrets GetSecrets(bool forceKeyVault = false);
    }
}
