﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Models.SFTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Repositories
{
    public interface IAgreementRepository
    {
        void CreateAgreement<T>(Agreement<T> agreement);
        Agreement<T> GetAgreement<T>(int agreementId);
        Agreement<T> GetAgreement<T>(string agreementName, AgreementType agreementType);

        void LogSuccessfulAuthentication(string agreementName, DateTime lastUsedUtcDate, int failedAuthenticationAttemptCount);
        void LogFailedAuthentication(string agreementName, int failedAuthenticationAttemptCount, bool lockedOut, DateTime? lastLockedOutUtcDate);
        void UnlockAgreement(int agreementId);
        void UpdateAgreement<T>(Agreement<T> agreement);
    }
}
