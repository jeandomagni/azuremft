﻿using AzureMFT.Common.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Repositories
{
    public interface ICacheRepository
    {
        byte[] Get(string key);
        void Set(string key, byte[] value, DistributedCacheEntryOptions options);
        bool Remove(string key);
    }
}
