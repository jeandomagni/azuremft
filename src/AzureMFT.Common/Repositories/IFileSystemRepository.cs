﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Result;
using AzureMFT.Common.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Repositories
{
    public interface IFileSystemRepository
    {
        PathListingItem GetFileAttributes(string path, IAgreement agreement);
        List<PathListingItem> GetPathListing(string path, IAgreement agreement);
        GenericResult<Stream> GetDownloadStream(string path, IAgreement agreement);
        GenericResult<Stream> GetUploadStream(string path, IAgreement agreement);
        GenericResult<Stream> GetUploadStream(string path, string subFolder, IAgreement agreement);
        ValidationResult RenameFile(string sourcePath, string targetPath, IAgreement agreement);
        ValidationResult DeleteFile(string path, IAgreement agreement);
    }
}
