﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Repositories
{
    public interface IRelayRepository
    {
        Task QueueRelayMessageAsync(RelayMessage queueMessage);
        Task QueueAS2FileReceivedMessageAsync(RelayMessage queueMessage);
        Task QueueAS2MDNMessageAsync(AS2MessageDispositionNotice queueMessage);
    }
}
