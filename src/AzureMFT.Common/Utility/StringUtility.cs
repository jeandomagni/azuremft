﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Utility
{
    public static class StringUtility
    {
        /// <summary>
        /// Removes all whitespace.
        /// </summary>
        [DebuggerStepThrough]
        public static string RemoveWhitespace(this string instance)
        {
            return RegexLibrary.WhitespaceRegex.Replace(instance, string.Empty);
        }
    }
}
