﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Utility
{
    public class TemporaryFileStreamManager : IDisposable
    {
        private Dictionary<string, Stream> _trackedStreams;
        private string _tempPath;

        public TemporaryFileStreamManager(string tempPath)
        {
            Directory.CreateDirectory(tempPath);

            _trackedStreams = new Dictionary<string, Stream>();
            _tempPath = tempPath;
        }

        public Stream NewStream()
        {
            var tempFile = Path.Combine(_tempPath, Path.GetRandomFileName());
            var stream = new FileStream(tempFile, FileMode.Create, FileAccess.ReadWrite);
            _trackedStreams.Add(tempFile, stream);
            return stream;
        }

        public void Dispose()
        {
            foreach (var trackedStream in _trackedStreams)
            {
                trackedStream.Value.Dispose();
                File.Delete(trackedStream.Key);
            }
        }
    }
}
