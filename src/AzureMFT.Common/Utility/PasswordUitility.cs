﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Utility
{
    public static class PasswordUtility
    {
        private static readonly string HashType;

        static PasswordUtility()
        {
            HashType = "SHA1";
        }

        public static string GenerateSalt()
        {
            byte[] buf = new byte[16];
            (new RNGCryptoServiceProvider()).GetBytes(buf);
            return Convert.ToBase64String(buf);
        }

        public static string GenerateHash(string pass, string salt)
        {
            return GenerateHash(pass, ref salt);
        }

        public static string GenerateHash(string pass, ref string salt)
        {
            if (string.IsNullOrEmpty(salt))
                salt = PasswordUtility.GenerateSalt();

            byte[] bIn = Encoding.Unicode.GetBytes(pass);
            byte[] bSalt = Convert.FromBase64String(salt);
            byte[] bAll = new byte[bSalt.Length + bIn.Length];

            Buffer.BlockCopy(bSalt, 0, bAll, 0, bSalt.Length);
            Buffer.BlockCopy(bIn, 0, bAll, bSalt.Length, bIn.Length);

            HashAlgorithm s = HashAlgorithm.Create(HashType);
            byte[] bRet = s.ComputeHash(bAll);

            return Convert.ToBase64String(bRet);
        }

        static string lower { get { return "abcdefghijklmnopqrstuvwzyz"; } }
        static string numbers { get { return "0123456789"; } }
        static string specials { get { return "!@#$%^&*_+=-"; } }
        static char newChar = new char();

        public static string GenerateAlphanumericCode(int len = 6, int numNumeric = 3)
        {
            var code = new char[len];
            var filledIndexes = new HashSet<int>();
            var rand = new Random();
            Func<int> nextIndex = () => GetNextIndexNotYetUsed(filledIndexes, len, rand);

            var uppers = lower.ToUpper();

            for (var i = 0; i < numNumeric; i++)
                code[nextIndex()] = GetRandomCharFromString(numbers, rand);


            for (var i = 0; i < len; i++)
                if (code[i] == newChar)
                    code[i] = GetRandomCharFromString(uppers, rand);

            return new String(code);
        }

        public static string GeneratePassword(int len)
        {
            var password = new char[len];
            var filledIndexes = new HashSet<int>();
            var rand = new Random();
            Func<int> nextIndex = () => GetNextIndexNotYetUsed(filledIndexes, len, rand);

            // set an uppercase
            password[nextIndex()] = GetRandomCharFromString(lower.ToUpper(), rand);
            // set a number
            password[nextIndex()] = GetRandomCharFromString(numbers, rand);
            // set a special character
            password[nextIndex()] = GetRandomCharFromString(specials, rand);

            for (var i = 0; i < len; i++)
            {
                if (password[i] == newChar)
                    password[i] = GetRandomCharFromString(lower, rand);
            }

            return new String(password);
        }

        static int GetNextIndexNotYetUsed(HashSet<int> filledIndexes, int maxValue, Random rand)
        {
            int index;
            do
            {
                index = rand.Next(maxValue - 1);
            }
            while (filledIndexes.Contains(index));

            filledIndexes.Add(index);

            return index;
        }

        static char GetRandomCharFromString(string str, Random rand)
        {
            var index = rand.Next(str.Length - 1);
            return str[index];
        }
    }
}
