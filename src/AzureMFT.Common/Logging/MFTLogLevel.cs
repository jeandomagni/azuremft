﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging
{
    public enum MFTLogLevel
    {
        /// <summary>
        /// Logs exceptions only
        /// </summary>
        Error = 0,

        /// <summary>
        /// Logs errors and actions 
        /// </summary>
        Action = 1,

        /// <summary>
        /// Logs all chatter between server and client
        /// </summary>
        Verbose = 2
    }
}
