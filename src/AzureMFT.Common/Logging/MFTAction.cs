﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging
{
    public enum MFTAction
    {
        LoginSuccessful,
        LoginFailure,
        UploadStarted,
        UploadComplete,
        Delete,
        Download,
        Listing,
        Rename,
        ChangeDirectory
    }
}
