﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging
{
    public class IPBannedEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "IP Banned";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public IPBannedEvent(string ipAddress)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add("IPAddress", ipAddress);

            Properties = eventData;
        }
    }
}
