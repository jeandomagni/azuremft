﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging
{
    public enum LogLevel
    {
        Error = 1,
        Warning = 2,
        Debug = 3,
        Information = 4,
    }
}
