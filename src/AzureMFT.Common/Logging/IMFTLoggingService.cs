﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging
{
    public interface IMFTLoggingService
    {
        void LogAction(string agreementName, MFTAction action, string message);
    }
}
