﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging
{
    public interface ICustomLogEvent
    {
        string EventName { get; }
        IDictionary<string, string> Properties { get; }
    }
}
