﻿using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class FileReceivedEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "FileReceived";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public FileReceivedEvent(IAgreement agreement, string filePath, long bytesTransferred, string messageId = null)
        {
            var eventData = new Dictionary<string, string>();
            eventData.AddRange(agreement.GetPropertiesFormat());
            eventData.Add("File", filePath);
            eventData.Add("BytesTransferred", bytesTransferred.ToString());
            if (!string.IsNullOrWhiteSpace(messageId))
                eventData.Add("MessageId", messageId);

            Properties = eventData;
        }
    }
}
