﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class TemporaryAS2FileReceivedEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "TemporaryAS2FileReceived";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public TemporaryAS2FileReceivedEvent(IAgreement agreement, string filePath, string messageId = null)
        {
            var eventData = new Dictionary<string, string>();
            eventData.AddRange(agreement.GetPropertiesFormat());
            eventData.Add("File", filePath);
            if (!string.IsNullOrWhiteSpace(messageId))
                eventData.Add("MessageId", messageId);

            Properties = eventData;
        }
    }
}
