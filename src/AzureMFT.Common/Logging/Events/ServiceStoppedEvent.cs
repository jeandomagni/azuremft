﻿using AzureMFT.Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class ServiceStoppedEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "ServiceStopped";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public ServiceStoppedEvent(string serviceName)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add("ServiceName", serviceName);

            Properties = eventData;
        }
    }
}
