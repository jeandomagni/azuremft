﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class AuthenticationSuccessEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "AuthenticationSuccess";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public AuthenticationSuccessEvent(string agreementName, string ipAddress)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add("AgreementName", agreementName);
            eventData.Add("IPAddress", ipAddress);

            Properties = eventData;
        }
    }
}
