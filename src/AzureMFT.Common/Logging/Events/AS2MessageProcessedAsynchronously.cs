﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class AS2MessageProcessedAsynchronously : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "AS2MessageProcessedAsynchronously";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public AS2MessageProcessedAsynchronously(IAgreement agreement, string filePath, string messageId = null)
        {
            var eventData = new Dictionary<string, string>();
            eventData.AddRange(agreement.GetPropertiesFormat());
            eventData.Add("File", filePath);
            if (!string.IsNullOrWhiteSpace(messageId))
                eventData.Add("MessageId", messageId);

            Properties = eventData;
        }
    }
}
