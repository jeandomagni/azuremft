﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class FileSentEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "FileSent";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public FileSentEvent(IAgreement agreement, string filePath, long bytesTransferred, string messageId = null)
        {
            var eventData = new Dictionary<string, string>();
            eventData.AddRange(agreement.GetPropertiesFormat());
            eventData.Add("File", filePath);
            eventData.Add("BytesTransferred", bytesTransferred.ToString());

            Properties = eventData;
        }
    }
}
