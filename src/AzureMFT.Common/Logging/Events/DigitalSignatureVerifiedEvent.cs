﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class DigitalSignatureVerifiedEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "DigitalSignatureVerified";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public DigitalSignatureVerifiedEvent(IAgreement agreement, string publicKeyAlgorithm, string digestAlgorithm, string messageId = null)
        {
            var eventData = new Dictionary<string, string>();
            eventData.AddRange(agreement.GetPropertiesFormat());
            eventData.Add("PublicKeyAltorithm", publicKeyAlgorithm);
            eventData.Add("DigestAlgorithm", digestAlgorithm);
            if(!string.IsNullOrWhiteSpace(messageId))
                eventData.Add("MessageId", messageId);

            Properties = eventData;
        }
    }
}
