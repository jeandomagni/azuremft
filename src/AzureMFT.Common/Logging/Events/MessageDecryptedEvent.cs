﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class MessageDecryptedEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "MessageDecrypted";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public MessageDecryptedEvent(IAgreement agreement, string messageId = null)
        {
            var eventData = new Dictionary<string, string>();
            eventData.AddRange(agreement.GetPropertiesFormat());
            if (!string.IsNullOrWhiteSpace(messageId))
                eventData.Add("MessageId", messageId);

            Properties = eventData;
        }
    }
}
