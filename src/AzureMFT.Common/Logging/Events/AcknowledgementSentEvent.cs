﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class AcknowledgementSentEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "AcknowledgementSent";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public AcknowledgementSentEvent(string originalMessageId, string messageId, string endpoint, int responseStatusCode)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add("OriginalMessageId", originalMessageId);
            eventData.Add("MessageId", messageId);
            eventData.Add("Endpoint", endpoint);
            eventData.Add("ResponseStatusCode", responseStatusCode.ToString());

            Properties = eventData;
        }
    }
}
