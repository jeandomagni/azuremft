﻿using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class ServiceStartedEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "ServiceStarted";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }

        public ServiceStartedEvent(string serviceName)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add("ServiceName", serviceName);

            Properties = eventData;
        }
    }
}
