﻿using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging.Events
{
    public class AuthenticationFailureEvent : ICustomLogEvent
    {
        public string EventName
        {
            get
            {
                return "AuthenticationFailure";
            }
        }

        public IDictionary<string, string> Properties { get; private set; }


        public AuthenticationFailureEvent(string agreementName, AgreementType agreementType, string ipAddress, AuthenticationFailureReason reason)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add("AgreementName", agreementName);
            eventData.Add("AgreementType", agreementType.ToString());
            eventData.Add("IPAddress", ipAddress);
            eventData.Add("Reason", reason.ToString());

            Properties = eventData;
        }
    }

    public enum AuthenticationFailureReason
    {
        PasswordMisMatch,
        AgreementLockedOut,
        AgreementNameNotFound,
        PublicKeyMisMatch
    }
}
