﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Logging
{
    public interface ILoggingService
    {
        void LogException(Exception ex);
        void LogException(Exception ex, string agreementName, Dictionary<string, string> properties);
        void LogError(string message, Exception ex);
        void LogError(string message, Dictionary<string, string> properties = null);
        void LogWarning(string message);
        void LogDebug(string message);
        void LogInformation(string message);
        void LogCriticalInformation(string message);
        void LogCustom(ICustomLogEvent logEvent);
        bool HasRequiredLogLevel(LogLevel logLevel);
        void Flush();
    }
}
