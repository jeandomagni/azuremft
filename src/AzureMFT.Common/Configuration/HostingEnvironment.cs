﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Configuration
{
    public class HostingEnvironment
    {
        public HostingEnvironment(string environmentName)
        {
            EnvironmentName = environmentName;
        }

        public string EnvironmentName { get; private set; }

        public bool IsDevelopment()
        {
            return string.Equals(EnvironmentName, "Development");
        }

        public bool IsStaging()
        {
            return string.Equals(EnvironmentName, "Staging");
        }

        public bool IsProduction()
        {
            return string.Equals(EnvironmentName, "Production");
        }
    }
}
