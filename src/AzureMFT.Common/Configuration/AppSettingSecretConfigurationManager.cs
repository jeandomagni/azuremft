﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureMFT.Common.Models;

namespace AzureMFT.Common.Configuration
{
    public class AppSettingSecretConfigurationManager : ISecretConfigurationManager
    {
        public string GetSecret(Secrets.Names secret)
        {
            return System.Configuration.ConfigurationManager.AppSettings[secret.ToString()];
        }
    }
}
