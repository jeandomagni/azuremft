﻿using AzureMFT.Common.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;

namespace AzureMFT.Common.Configuration
{
    public class ConfigurationManager
    {
        public string ADClientId { get; private set; }
        public string ADClientSecret { get; private set; }
        public string ApplicationInsightsInstrumentationKey { get; private set; }
        public string AS2FileReceivedQueueName { get; private set; }
        public string AS2Id { get; private set; }
        public string AS2MDNDispatchQueueName { get; private set; }
        public string AS2PublicCertificateAlias { get; set; }
        public string AS2PublicCertificateKey { get; set; }
        public string AS2PublicCertificatePasswordKey { get; set; }
        public string AS2PublicCertificateVersion { get; set; }
        public HostingEnvironment Environment { get; private set; }
        public int FailedAuthenticationAttemptLimit { get; private set; }
        public LogLevel LogLevel { get; private set; }
        public string RelayQueueName { get; set; }
        public string SFTPHostCertificateKey { get; set; }
        public string SFTPHostCertificatePasswordKey { get; set; }
        public string SFTPHostCertificateVersion { get; set; }
        public string TempPath { get; set; }
        public string VaultName { get; private set; }

        public ConfigurationManager(ConfigurationManagerBuilder builder)
        {
            ADClientId = builder.ADClientId;
            ADClientSecret = builder.ADClientSecret;
            ApplicationInsightsInstrumentationKey = builder.ApplicationInsightsInstrumentationKey;
            AS2Id = builder.AS2Id;
            AS2PublicCertificateAlias = builder.AS2PublicCertificateAlias;
            AS2PublicCertificateKey = builder.AS2PublicCertificateKey;
            AS2PublicCertificatePasswordKey = builder.AS2PublicCertificatePasswordKey;
            AS2PublicCertificateVersion = builder.AS2PublicCertificateVersion;
            Environment = builder.Environment;
            FailedAuthenticationAttemptLimit = builder.FailedAuthenticationAttemptLimit;
            LogLevel = builder.LogLevel;
            RelayQueueName = builder.RelayQueueName;
            AS2FileReceivedQueueName = builder.AS2FileReceivedQueueName;
            AS2MDNDispatchQueueName = builder.AS2MDNDispatchQueueName;
            SFTPHostCertificateKey = builder.SFTPHostCertificateKey;
            SFTPHostCertificatePasswordKey = builder.SFTPHostCertificatePasswordKey;
            SFTPHostCertificateVersion = builder.SFTPHostCertificateVersion;
            TempPath = builder.TempPath;
            VaultName = builder.VaultName;
        }
    }
}
