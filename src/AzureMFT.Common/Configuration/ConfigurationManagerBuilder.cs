﻿using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Configuration
{
    public class ConfigurationManagerBuilder
    {
        public string ADClientId { get; set; }
        public string ADClientSecret { get; set; }
        public string ApplicationInsightsInstrumentationKey { get; set; }
        public string AS2FileReceivedQueueName { get; set; }
        public string AS2Id { get; set; }
        public string AS2MDNDispatchQueueName { get; set; }
        public string AS2PublicCertificateAlias { get; set; }
        public string AS2PublicCertificateKey { get; set; }
        public string AS2PublicCertificatePasswordKey { get; set; }
        public string AS2PublicCertificateVersion { get; set; }
        public HostingEnvironment Environment { get; set; }
        public int FailedAuthenticationAttemptLimit { get; set; }
        public LogLevel LogLevel { get; set; }
        public string RelayQueueName { get; set; }
        public string SFTPHostCertificateKey { get; set; }
        public string SFTPHostCertificatePasswordKey { get; set; }
        public string SFTPHostCertificateVersion { get; set; }
        public string TempPath { get; set; }
        public string VaultName { get; set; }

        public ConfigurationManager Build()
        {
            return new ConfigurationManager(this);
        }
    }
}
