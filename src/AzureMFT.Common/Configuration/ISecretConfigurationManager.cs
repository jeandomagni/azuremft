﻿using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Configuration
{
    public interface ISecretConfigurationManager
    {
        string GetSecret(Secrets.Names secret);
    }
}
