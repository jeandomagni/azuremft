﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Services
{
    public interface ICacheService
    {
        byte[] Get(string key);
        T Get<T>(string key);
        bool Remove(string key);
        void Set(string key, byte[] value, TimeSpan expiresIn);
        void Set<T>(string key, T value, TimeSpan expiresIn);
    }
}
