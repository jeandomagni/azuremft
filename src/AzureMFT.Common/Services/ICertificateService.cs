﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Common.Services
{
    public interface ICertificateService
    {
        Task<CertificateBundle> GetCertificateBundleAsync(Agreement<AS2AgreementProperties> agreement);
        Task<CertificateBundle> GetCertificateBundleAsync(string certificateKey, string certificatePasswordKey, string certificateVersion);
        Task<DataFile> GetPublicCertificateAsync();
    }
}
