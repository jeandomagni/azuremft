﻿using AzureMFT.Common.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureMFT.Common.Models;
using System.IO;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Models.AS2;

namespace AzureMFT.Console_Runner
{
    public class ConsoleRelayRepository : IRelayRepository
    {
        private readonly ILoggingService _logger;

        public ConsoleRelayRepository(ILoggingService logger)
        {
            _logger = logger;
        }

        public Task QueueAS2FileReceivedMessageAsync(RelayMessage queueMessage)
        {
            var queueTask = new Task(() =>
            {
                _logger.LogCriticalInformation(queueMessage.ToString());
            });
            queueTask.Start();

            return queueTask;
        }

        public Task QueueAS2MDNMessageAsync(AS2MessageDispositionNotice queueMessage)
        {
            var queueTask = new Task(() =>
            {
                _logger.LogCriticalInformation(queueMessage.ToString());
            });
            queueTask.Start();

            return queueTask;
        }

        public Task QueueRelayMessageAsync(RelayMessage message)
        {
            var queueTask = new Task(() =>
            {
                _logger.LogCriticalInformation(message.ToString());
            });
            queueTask.Start();

            return queueTask;
        }
    }
}
