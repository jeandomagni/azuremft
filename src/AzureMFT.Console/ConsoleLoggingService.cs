﻿using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Console_Runner
{
    public class ConsoleLoggingService : ILoggingService
    {
        private readonly ConfigurationManager _config;
        private static object _writerLock = new object();

        public ConsoleLoggingService(ConfigurationManager config)
        {
            _config = config;
        }

        public void Flush()
        {
        }

        public bool HasRequiredLogLevel(LogLevel logLevel)
        {
            return _config.LogLevel >= logLevel;
        }

        public void LogCriticalInformation(string message)
        {
            WriteLine(new ConsoleLine(message, ConsoleColor.Magenta));
        }

        public void LogCustom(ICustomLogEvent logEvent)
        {
            var lines = new List<ConsoleLine>();
            lines.Add(new ConsoleLine(logEvent.EventName + ":"));
            foreach (var property in logEvent.Properties)
            {
                lines.Add(new ConsoleLine($"{property.Key}: {property.Value}"));
            }
            WriteLines(lines);
        }

        public void LogDebug(string message)
        {
            if (_config.LogLevel >= LogLevel.Debug)
            {
                WriteLine(new ConsoleLine(message));
            }
        }

        public void LogError(string message, Exception ex)
        {
            if (_config.LogLevel >= LogLevel.Error)
            {
                var lines = new List<ConsoleLine>();
                lines.Add(new ConsoleLine(message, ConsoleColor.Red));
                lines.Add(new ConsoleLine(ex.ToString()));
                WriteLines(lines);
            }
        }

        public void LogError(string message, Dictionary<string, string> properties = null)
        {
            if (_config.LogLevel >= LogLevel.Error)
            {
                if (properties != null)
                {
                    var lines = new List<ConsoleLine>();
                    lines.Add(new ConsoleLine(message, ConsoleColor.Red));
                    foreach (var property in properties)
                    {
                        lines.Add(new ConsoleLine($"{property.Key}: {property.Value}"));
                    }
                    WriteLines(lines);
                }
                else
                    WriteLine(new ConsoleLine(message, ConsoleColor.Red));
            }
        }

        public void LogException(Exception ex)
        {
            WriteLine(new ConsoleLine(ex.ToString(), ConsoleColor.Red));
        }

        public void LogException(Exception ex, string userName, Dictionary<string, string> properties)
        {
            WriteLine(new ConsoleLine(ex.ToString(), ConsoleColor.Red));
        }

        public void LogInformation(string message)
        {
            if (_config.LogLevel >= LogLevel.Information)
            {
                WriteLine(new ConsoleLine(message));
            }
        }

        public void LogWarning(string message)
        {
            if (_config.LogLevel >= LogLevel.Warning)
            {
                WriteLine(new ConsoleLine(message, ConsoleColor.Yellow));
            }
        }

        private void WriteLine(ConsoleLine line)
        {
            lock (_writerLock)
            {
                Console.WriteLine();
                Console.ForegroundColor = line.Color;
                Console.WriteLine(line.Line);
                Console.ResetColor();
            }
        }

        private void WriteLines(IEnumerable<ConsoleLine> lines)
        {
            lock(_writerLock)
            {
                Console.WriteLine();
                foreach (var line in lines)
                {
                    Console.ForegroundColor = line.Color;
                    Console.WriteLine(line.Line);
                }
                Console.ResetColor();
            }
        }

        private class ConsoleLine
        {
            public string Line { get; private set; }
            public ConsoleColor Color { get; private set; }

            public ConsoleLine(string line, ConsoleColor color = ConsoleColor.White)
            {
                Line = line;
                Color = color;
            }
        }


    }
}
