﻿using Autofac;
using AzureMFT.Common;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Services;
using AzureMFT.Server;
using AzureMFT.Server.AS2;
using AzureMFT.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using SysConfig = System.Configuration;

namespace AzureMFT.Console_Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = RegisterIOC();

            var services = container.Resolve<IEnumerable<IMFTService>>();
            var endpoints = new Dictionary<string, IPEndPoint>()
            {
                { "AS2Standard", new IPEndPoint(IPAddress.Parse("127.0.0.1"), 4080) },
                { "AS2Secure", new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5080) },
                { "SFTP", new IPEndPoint(IPAddress.Parse("127.0.0.1"), 22) },
                { "APISSL", new IPEndPoint(IPAddress.Parse("127.0.0.1"), 443) },
                { "LB", new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8888) }
            };

            foreach (var service in services)
            {
                service.Start(container, endpoints);
            }

            while(true)
            {
                Thread.Sleep(10000);
            }
        }

        private static IContainer RegisterIOC()
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(DIRegistrationHelper.GetRegistrationAssemblies());

            var config = GetConfiguration();
            builder.RegisterInstance(config).SingleInstance();

            // Development specific overrides
            if(config.Environment.IsDevelopment())
            {
                builder.RegisterType<ConsoleLoggingService>().As<ILoggingService>().SingleInstance();
                builder.RegisterType<ConsoleRelayRepository>().As<IRelayRepository>().SingleInstance();

                var thumbprint = SysConfig.ConfigurationManager.AppSettings["DevRootCertificateThumbprint"];
                var password = SysConfig.ConfigurationManager.AppSettings["DevRootCertificatePassword"];
                builder.RegisterType<LocalCertificateService>().As<ICertificateService>()
                                                               .WithParameter("rootCertificateThumbprint", thumbprint)
                                                               .WithParameter("rootCertificatePassword", password);

                // TODO: Remove this
                builder.RegisterType<LocalAS2MDNService>().As<IMFTService>().SingleInstance();

                builder.RegisterType<AppSettingSecretConfigurationManager>().As<ISecretConfigurationManager>().SingleInstance();
                builder.RegisterType<MemoryCacheRepository>().As<ICacheRepository>().SingleInstance();
            }
            else
            {
                // TODO: Fix this by deleting the LocalAS2MDNService and extracting the service bus stuff so that it can be swapped out
                builder.RegisterType<AS2MDNService>().As<IMFTService>().SingleInstance();
            }

            return builder.Build();
        }

        private static ConfigurationManager GetConfiguration()
        {
            var builder = new ConfigurationManagerBuilder();
            var appSettings = SysConfig.ConfigurationManager.AppSettings;

            builder.ApplicationInsightsInstrumentationKey = appSettings["ApplicationInsightsInstrumentationKey"];
            builder.AS2Id = appSettings["AS2Id"];
            builder.AS2PublicCertificateAlias = appSettings["AS2PublicCertificateAlias"];
            builder.Environment = new HostingEnvironment(appSettings["Environment"]);
            builder.FailedAuthenticationAttemptLimit = int.Parse(appSettings["FailedAuthenticationAttemptLimit"]);
            builder.LogLevel = (LogLevel)Enum.Parse(typeof(LogLevel), appSettings["LogLevel"]);
            builder.TempPath = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "temp"), "as2temp");

            return builder.Build();
        }
    }
}
