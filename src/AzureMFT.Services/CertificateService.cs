﻿using AzureMFT.Common;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Services
{
    public class CertificateService : ICertificateService
    {
        private readonly ISecretVaultRepository _secretVaultRepository;
        private readonly ICacheService _cache;
        private readonly ConfigurationManager _config;
        private readonly ILoggingService _logger;

        public CertificateService(ISecretVaultRepository secretVaultRepository, ICacheService cache, ConfigurationManager config, ILoggingService logger)
        {
            _secretVaultRepository = secretVaultRepository;
            _cache = cache;
            _config = config;
            _logger = logger;
        }

        public async Task<CertificateBundle> GetCertificateBundleAsync(Agreement<AS2AgreementProperties> agreement)
        {
            return await GetCertificateBundleAsync(agreement.AgreementProperties.RootCertificateKey, 
                                                   agreement.AgreementProperties.RootCertificatePasswordKey,
                                                   agreement.AgreementProperties.RootCertificateVersion);
        }

        public async Task<CertificateBundle> GetCertificateBundleAsync(string certificateKey, string certificatePasswordKey, string certificateVersion)
        {
            var certificateSet = _cache.Get<Dictionary<string, CertificateBundle>>(certificateKey);
            CertificateBundle bundle = null;
            if (certificateSet != null && certificateSet.ContainsKey(certificateVersion))
                return certificateSet[certificateVersion];
            else
            {
                // This will throw an exception if the certificate does not exist.
                bundle = await _secretVaultRepository.GetCertificateBundleAsync(certificateKey, certificatePasswordKey, certificateVersion);

                if (certificateSet == null)
                {
                    certificateSet = new Dictionary<string, CertificateBundle>();
                }

                certificateSet.Add(certificateVersion, bundle);
                _cache.Set(certificateKey, certificateSet, TimeSpan.FromDays(15));
            }

            return bundle;
        }

        public async Task<DataFile> GetPublicCertificateAsync()
        {
            var bundle = await GetCertificateBundleAsync(_config.AS2PublicCertificateKey, _config.AS2PublicCertificatePasswordKey, _config.AS2PublicCertificateVersion);
            var result = new DataFile();
            result.ContentType = "application/pkix-cert";
            result.FileName = $"{_config.AS2PublicCertificateAlias}.cer";

            using (var stream = new MemoryStream(bundle.Certificate))
            {
                var store = new Org.BouncyCastle.Pkcs.Pkcs12Store(stream, bundle.CertificatePassword.ToArray());
                var x509 = store.GetCertificate(_config.AS2PublicCertificateAlias);

                using (var stringWriter = new StringWriter())
                {
                    var pemWriter = new Org.BouncyCastle.OpenSsl.PemWriter(stringWriter);
                    pemWriter.WriteObject(x509.Certificate);
                    result.Data = Encoding.UTF8.GetBytes(stringWriter.ToString());
                }
            }

            return result;
        }
    }
}
