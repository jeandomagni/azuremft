﻿using AzureMFT.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using System.Security.Cryptography.X509Certificates;

namespace AzureMFT.Services
{
    public class LocalCertificateService : ICertificateService
    {
        private readonly string _rootCertificateThumbprint;
        private readonly string _rootCertificatePassword;

        public LocalCertificateService(string rootCertificateThumbprint, string rootCertificatePassword)
        {
            _rootCertificateThumbprint = rootCertificateThumbprint;
            _rootCertificatePassword = rootCertificatePassword;
        }

        public Task<DataFile> GetPublicCertificateAsync()
        {
            return Task.Run(() =>
            {
                var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly);

                var certList = store.Certificates.Find(X509FindType.FindByThumbprint, _rootCertificateThumbprint, false);

                if (certList.Count == 0)
                    throw new KeyNotFoundException("Could not find the certificate with the name provided");

                var result = new DataFile();
                result.ContentType = "application/pkix-cert";
                result.FileName = $"{certList[0].FriendlyName}.cer";

                result.Data = certList[0].GetRawCertData();

                return result;
            });
        }

        public Task<CertificateBundle> GetCertificateBundleAsync(Agreement<AS2AgreementProperties> agreement)
        {
            return GetCertificateBundleAsync(agreement.AgreementProperties.RootCertificateKey, agreement.AgreementProperties.RootCertificatePasswordKey, agreement.AgreementProperties.RootCertificateVersion);
        }

        public Task<CertificateBundle> GetCertificateBundleAsync(string certificateKey, string certificatePasswordKey, string certificateVersion)
        {
            return Task.Run(() =>
            {
                var bundle = new CertificateBundle();

                var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly);

                var certList = store.Certificates.Find(X509FindType.FindByThumbprint, _rootCertificateThumbprint, false);

                if (certList.Count == 0)
                    throw new KeyNotFoundException("Could not find the certificate with the name provided");

                bundle.Certificate = certList[0].Export(X509ContentType.Pkcs12, _rootCertificatePassword);

                bundle.CertificatePassword = _rootCertificatePassword;

                return bundle;
            });
        }
    }
}
