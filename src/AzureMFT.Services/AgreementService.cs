﻿using System;
using System.Collections.Generic;
using System.Linq;
using AzureMFT.Common.Models;
using AzureMFT.Common.Result;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Utility;
using AzureMFT.Common.Validation;
using System.Collections.Concurrent;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Models.SFTP;
using AzureMFT.Common.Logging.Events;
using AzureMFT.Common.Services;
using AzureMFT.Common;

namespace AzureMFT.Services
{
    public class AgreementService
    {
        private readonly IAgreementRepository _agreementRepository;
        private readonly IBanListRepository _banListRepository;
        private readonly ICacheService _cache;
        private readonly ICacheService _memoryCache;
        private readonly ConfigurationManager _config;
        private readonly ILoggingService _logger;

        private ConcurrentDictionary<string, int> _banTracker = new ConcurrentDictionary<string, int>();
        private readonly HashSet<string> _probeList = new HashSet<string> { "13.64.128.11", "13.64.168.11", "13.64.88.11", "40.86.112.11", "40.86.40.11", "13.67.160.11", "13.95.160.11", "13.78.64.11", "40.69.0.11", "13.75.0.11" };

        public AgreementService(IAgreementRepository agreementRepository, IBanListRepository banListRepository, ICacheService cache, ConfigurationManager config, ILoggingService logger)
        {
            _agreementRepository = agreementRepository;
            _banListRepository = banListRepository;
            _cache = cache;
            _memoryCache = new CacheService(new MemoryCacheRepository());
            _config = config;
            _logger = logger;
        }

        #region CacheKeys

        private string GetBanListCacheKey()
        {
            return "BanList";
        }

        private string GetAgreementCacheKey(string agreementName, AgreementType agreementType)
        {
            return $"Agreement:{agreementName}:{agreementType}";
        }

        private string GetAgreementCacheKey(int agreementId)
        {
            return $"Agreement:ID:{agreementId}";
        }

        #endregion

        public GenericResult<Agreement<SFTPAgreementProperties>> Authenticate(string agreementName, byte[] publicKey, string password, string ipAddress)
        {
            var result = new GenericResult<Agreement<SFTPAgreementProperties>>();

            var agreement = GetAgreement<SFTPAgreementProperties>(agreementName, AgreementType.SFTP);

            if (result.IsValid && agreement == null)
            {
                result.AddViolation("agreementName", "Credentials are invalid");
            }

            if (result.IsValid && agreement.LockedOut)
            {
                result.AddViolation("agreementName", "Agreement is locked out.");
                _logger.LogCustom(new AuthenticationFailureEvent(agreementName, AgreementType.SFTP, ipAddress, AuthenticationFailureReason.AgreementLockedOut));
            }

            // Check password or public key
            if (result.IsValid)
            {
                var authMatch = false;
                if (agreement.AgreementProperties.PartnerSSHKey != null && !publicKey.IsNullOrEmpty())
                {
                    authMatch = agreement.AgreementProperties.PartnerSSHKey.SequenceEqual(publicKey);
                    _logger.LogCustom(new AuthenticationFailureEvent(agreementName, AgreementType.SFTP, ipAddress, AuthenticationFailureReason.PublicKeyMisMatch));
                }

                if (agreement.AgreementProperties.Password != null)
                {
                    authMatch = agreement.AgreementProperties.Password == PasswordUtility.GenerateHash(password, agreement.AgreementProperties.PasswordSalt);
                    _logger.LogCustom(new AuthenticationFailureEvent(agreementName, AgreementType.SFTP, ipAddress, AuthenticationFailureReason.PasswordMisMatch));
                }

                if (!authMatch)
                {
                    result.AddViolation("agreementName", "Credentials are invalid.");
                    agreement.FailedAuthenticationAttemptCount++;
                    if (agreement.FailedAuthenticationAttemptCount >= _config.FailedAuthenticationAttemptLimit)
                    {
                        agreement.LockedOut = true;
                        agreement.LastLockedOutUtcDate = DateTime.UtcNow;
                    }

                    _agreementRepository.LogFailedAuthentication(agreement.AgreementName, agreement.FailedAuthenticationAttemptCount, agreement.LockedOut, agreement.LastLockedOutUtcDate);
                }
            }

            if (result.IsValid)
            {
                result.Payload = agreement;
                _logger.LogCustom(new AuthenticationSuccessEvent(agreement.AgreementName, ipAddress));

                if (_banTracker.ContainsKey(ipAddress))
                {
                    int tryCount;
                    _banTracker.TryRemove(ipAddress, out tryCount);
                }
            }
            else
            {
                if (!_banTracker.ContainsKey(ipAddress))
                    _banTracker.TryAdd(ipAddress, 1);
                else
                {
                    int current;
                    _banTracker.TryGetValue(ipAddress, out current);
                    _banTracker.TryUpdate(ipAddress, current + 1, current);
                }
            }

            return result;
        }

        public GenericResult<Agreement<AS2AgreementProperties>> Authenticate(AS2Message as2Message, string ipAddress)
        {
            var result = new GenericResult<Agreement<AS2AgreementProperties>>();
            // Check to see if the incoming message is even for us
            if (!as2Message.AS2To.Equals(_config.AS2Id, StringComparison.CurrentCultureIgnoreCase))
                result.AddViolation("AS2Id", $"The message is meant for {as2Message.AS2To} not for us {_config.AS2Id}.");

            if(result.IsValid)
            {
                var agreement = GetAgreement<AS2AgreementProperties>(as2Message.AS2From, AgreementType.AS2);

                // If the agreement cannot be found, we cannot continue
                if (agreement == null)
                {
                    result.AddViolation("AS2From", $"There is no agreement for {as2Message.AS2From}.");
                    _logger.LogCustom(new AuthenticationFailureEvent(as2Message.AS2From, AgreementType.AS2, ipAddress, AuthenticationFailureReason.AgreementNameNotFound));
                }

                if(result.IsValid)
                {
                    result.Payload = agreement;
                    _logger.LogCustom(new AuthenticationSuccessEvent(agreement.AgreementName, ipAddress));
                }
            }

            return result;
        }

        //TODO: CreateAgreement method. Make sure to validate the agreement properties

        public ValidationResult ShouldAcceptConnection(string ipAddress)
        {
            var result = new ValidationResult();

            if (_probeList.Contains(ipAddress))
                result.AddViolation("ipAddress", "Probe");

            var banList = GetBanList();

            if (banList.Contains(ipAddress))
                result.AddViolation("ipAddress", "Banned");

            if (result.IsValid && _banTracker.ContainsKey(ipAddress) && _banTracker[ipAddress] >= 20)
            {
                _banListRepository.AddToBanList(ipAddress);
                _logger.LogCustom(new IPBannedEvent(ipAddress));

                _cache.Remove(GetBanListCacheKey());
                _memoryCache.Remove(GetBanListCacheKey());

                int tryCount;
                _banTracker.TryRemove(ipAddress, out tryCount);
            }

            return result;
        }

        public Agreement<T> GetAgreement<T>(int agreementId)
        {
            var cacheKey = GetAgreementCacheKey(agreementId);

            var agreement = _memoryCache.Get<Agreement<T>>(cacheKey);
            if (agreement == null)
            {
                agreement = _agreementRepository.GetAgreement<T>(agreementId);
                if (agreement != null)
                {
                    _cache.Set(cacheKey, agreement, TimeSpan.FromDays(10));
                }
            }

            return agreement;
        }

        public Agreement<T> GetAgreement<T>(string agreementName, AgreementType agreementType)
        {
            var cacheKey = GetAgreementCacheKey(agreementName, agreementType);

            // First try to get the agreement from memory cache
            // We use memory cache here because we don't want distributed cache to 
            // get pounded if there is a flurry of connection attempts
            var agreement = _memoryCache.Get<Agreement<T>>(cacheKey);
            if (agreement == null)
            {
                // If not in memory, try getting from distributed cache
                agreement = _cache.Get<Agreement<T>>(cacheKey);
                if (agreement == null)
                {
                    agreement = _agreementRepository.GetAgreement<T>(agreementName, agreementType);
                    if (agreement != null)
                        _cache.Set(cacheKey, agreement, TimeSpan.FromDays(10));
                    else
                    {
                        // We can't find an agreement so put a dummy agreement in memory
                        // cache for a few seconds to mitigate flurry hits to the database
                        _memoryCache.Set(cacheKey, new Agreement<T>() { AgreementId = 0 }, TimeSpan.FromSeconds(10));
                    }
                }
            }
            else if (agreement.AgreementId == 0)
            {
                // This is another attempt to login with an agreement that doesn't exist
                return null;
            }


            return agreement;
        }

        private HashSet<string> GetBanList()
        {
            var cacheKey = GetBanListCacheKey();

            // First try to get the ban list from memory cache
            // We use memory cache here because we don't want distributed cache to 
            // get pounded if there is a flurry of connection attempts
            var banList = _memoryCache.Get<HashSet<string>>(cacheKey);
            if(banList == null)
            {
                // If not in memory, try getting from distributed cache
                banList = _cache.Get<HashSet<string>>(cacheKey);
                if(banList == null)
                {
                    banList = _banListRepository.GetBanList();
                    _cache.Set(cacheKey, banList, TimeSpan.FromDays(1));
                    _memoryCache.Set(cacheKey, banList, TimeSpan.FromSeconds(10));
                }
            }

            return banList;
        }

    }
}
