﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Exceptions;
using MimeKit.Cryptography;
using AzureMFT.Common.Result;
using System.IO;
using Org.BouncyCastle.X509;
using AzureMFT.Common.Logging;
using AzureMFT.Common.Models;
using MimeKit;
using MimeKit.Utils;
using AzureMFT.Common.Configuration;
using AzureMFT.Common.Services;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Logging.Events;
using System.Collections.Generic;
using AzureMFT.Common.Utility;
using AzureMFT.Common;

namespace AzureMFT.Services
{
    public class AS2ProcessingService
    {
        private readonly AgreementService _agreementService;
        private readonly ICertificateService _certificateService;
        private readonly FileSystemService _fileSystemService;
        private readonly IRelayRepository _relayRepository;
        private readonly ConfigurationManager _config;
        private readonly ILoggingService _logger;

        public readonly AS2Config AS2Config;
        public readonly ILoggingService Logger;

        private const DigestAlgorithm DIGESTALGORITHM = DigestAlgorithm.Sha256;


        public AS2ProcessingService(AgreementService agreementService, ICertificateService certificateService, FileSystemService fileSystemService, IRelayRepository relayRepository, ConfigurationManager config, ILoggingService logger)
        {
            _agreementService = agreementService;
            _certificateService = certificateService;
            _fileSystemService = fileSystemService;
            _relayRepository = relayRepository;
            _config = config;
            _logger = logger;

            AS2Config = new AS2Config() { AS2Id = _config.AS2Id, Environment = _config.Environment };
            Logger = _logger;
        }

        public async Task<bool> ProcessMessageAsynchronousMDNAsync(AS2Message as2Message, string ipAddress)
        {
            var result = false;
            var authResult = _agreementService.Authenticate(as2Message, ipAddress);

            if (!authResult.IsValid)
                throw new BadAS2RequestException(authResult.RuleViolations.First().ErrorMessage);

            var agreement = authResult.Payload;

            var fileName = "as2_processing/" + Path.GetRandomFileName();
            var uploadStreamResult = _fileSystemService.GetUploadStream(fileName, agreement);

            if (uploadStreamResult.IsValid)
            {
                var fullStream = new ConcatenatedStream(new List<Stream> { as2Message.HeaderStream, as2Message.ContentObject });
                using (var uploadStream = uploadStreamResult.Payload)
                {
                    await fullStream.CopyToAsync(uploadStreamResult.Payload);
                    _logger.LogCustom(new TemporaryAS2FileReceivedEvent(agreement, as2Message.MessageId, fileName));
                }
                await _relayRepository.QueueAS2FileReceivedMessageAsync(new RelayMessage(agreement.AgreementId, agreement.TradingPartner.TradingPartnerId, fileName, DateTime.UtcNow));
                result = true;
            }
            else
            {
                _logger.LogException(new BadAS2RequestException(uploadStreamResult.RuleViolations.First().ErrorMessage));
            }

            return result;
        }

        public async Task<AS2MessageDispositionNotice> ProcessMessageSyncronousMDNAsync(AS2Message as2Message, string ipAddress)
        {
            var authResult = _agreementService.Authenticate(as2Message, ipAddress);

            if(!authResult.IsValid)
                throw new BadAS2RequestException(authResult.RuleViolations.First().ErrorMessage);

            var agreement = authResult.Payload;

            var as2MessageStream = new ConcatenatedStream(new List<Stream> { as2Message.HeaderStream, as2Message.ContentObject });

            return await ProcessIncomingMessageAsync(as2MessageStream, agreement);
        }

        private bool ValidateCertificateBundle(CertificateBundle certificateBundle, Agreement<AS2AgreementProperties> agreement)
        {
            var result = true;
            if (certificateBundle.Certificate == null || certificateBundle.CertificatePassword == null)
            {
                var error = new Exception("Could not find secrets specified in the agreement.");
                error.Data.Add("AgreementId", agreement.AgreementId);
                if (certificateBundle.Certificate == null)
                {
                    error.Data.Add("RootCertificateKey", agreement.AgreementProperties.RootCertificateKey);
                    error.Data.Add("RootCertificateVersion", agreement.AgreementProperties.RootCertificateVersion);
                }
                if (certificateBundle.CertificatePassword == null)
                {
                    error.Data.Add("RootCertificatePasswordKey", agreement.AgreementProperties.RootCertificatePasswordKey);
                }
                _logger.LogException(error);

                result = false;
            }

            return result;
        }

        public async Task<DataFile> GetPublicCertificateAsync()
        {
            return await _certificateService.GetPublicCertificateAsync();
        }

        public async Task<AS2MessageDispositionNotice> ProcessIncomingMessageAsync(Stream as2MessageStream, Agreement<AS2AgreementProperties> agreement)
        {
            var result = new ProcessAS2MessageResult();
            MimeMessage message = null;
            result.DispositionMessage = "processed";
            var certificateBundle = await _certificateService.GetCertificateBundleAsync(agreement);

            // If the root certificate specified in the agreement can't be found,
            // we can't send back a proper MDN because it cannot be signed. Just error out.
            if (!ValidateCertificateBundle(certificateBundle, agreement))
                throw new BadAS2RequestException("Agreement not configured properly.");

            using (var mimeContext = new TemporarySecureMimeContext())
            using (var tempFileManager = new TemporaryFileStreamManager(_config.TempPath))
            {
                try
                {
                    var partnerFingerprint = new X509CertificateParser().ReadCertificate(agreement.AgreementProperties.PartnerCertificate).GetFingerprint();

                    mimeContext.Import(new MemoryStream(certificateBundle.Certificate), certificateBundle.CertificatePassword);

                    message = MimeMessage.Load(as2MessageStream, true);

                    await HandleMimeEntityAsync(message.Body, mimeContext, agreement, result, partnerFingerprint, message, tempFileManager);
                }
                catch (Exception ex)
                {
                    _logger.LogError(AS2ProcessorStatusMessages.UnexpectedProcessingError, ex);
                    result = new ProcessAS2MessageResult() { DispositionMessage = AS2ProcessorStatusMessages.UnexpectedProcessingError };
                }

                return BuildMDN(result, certificateBundle, mimeContext, message);
            }
        }

        private bool VerifySignatures(MultipartSigned signed, TemporarySecureMimeContext mimeContext, Agreement<AS2AgreementProperties> agreement, ProcessAS2MessageResult result, string partnerFingerprint, MimeMessage as2Message)
        {
            var errored = false;
            try
            {
                var signatures = signed.Verify(mimeContext);
                foreach (var signature in signatures)
                {
                    if (signature.Verify())
                    {
                        if (partnerFingerprint == signature.SignerCertificate.Fingerprint)
                        {
                            _logger.LogCustom(new DigitalSignatureVerifiedEvent(agreement, signature.PublicKeyAlgorithm.ToString(), signature.DigestAlgorithm.ToString(), as2Message.MessageId));
                            var signatureData = signature as SecureMimeDigitalSignature;
                            if (signatureData == null)
                                throw new Exception("Could not cast as a SecureMimeDigitalSignature");

                            result.MessageIntegrityCheck = Convert.ToBase64String(signatureData.SignerInfo.GetContentDigest());
                            result.MICAlg = MimeUtility.GetMICAlg(as2Message.Headers[HeaderId.DispositionNotificationOptions]);
                        }
                    }
                    else
                    {
                        result.DispositionMessage = AS2ProcessorStatusMessages.IntegrityCheckFailed;
                        errored = true;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is NotSupportedException)
                {
                    _logger.LogError(AS2ProcessorStatusMessages.UnsupportedMICAlgorithm, ex);
                    result.DispositionMessage = AS2ProcessorStatusMessages.UnsupportedMICAlgorithm;
                }
                else if (ex is FormatException)
                {
                    _logger.LogError(AS2ProcessorStatusMessages.UnsupportedFormat, ex);
                    result.DispositionMessage = AS2ProcessorStatusMessages.UnsupportedFormat;
                }
                else
                {
                    _logger.LogError(AS2ProcessorStatusMessages.UnexpectedProcessingError, ex);
                    result.DispositionMessage = AS2ProcessorStatusMessages.IntegrityCheckFailed;
                }

                errored = true;
            }

            return errored;
        }

        private async Task HandleMimeEntityAsync(MimeEntity entity, TemporarySecureMimeContext mimeContext, Agreement<AS2AgreementProperties> agreement, ProcessAS2MessageResult result, string partnerFingerprint, MimeMessage as2Message, TemporaryFileStreamManager tempFileManager)
        {
            var multipart = entity as Multipart;

            if (multipart != null)
            {
                if(multipart is MultipartSigned)
                {
                    var signed = multipart as MultipartSigned;
                    VerifySignatures(signed, mimeContext, agreement, result, partnerFingerprint, as2Message);
                }

                for (int i = 0; i < multipart.Count; i++)
                    await HandleMimeEntityAsync(multipart[i], mimeContext, agreement, result, partnerFingerprint, as2Message, tempFileManager);
                return;
            }

            var rfc822 = entity as MessagePart;

            if (rfc822 != null)
            {
                var message = rfc822.Message;

                await HandleMimeEntityAsync(message.Body, mimeContext, agreement, result, partnerFingerprint, as2Message, tempFileManager);
                return;
            }

            var part = (MimePart)entity;

            await ProcessMimePartAsync(part, mimeContext, agreement, result, partnerFingerprint, as2Message, tempFileManager);
        }

        private async Task ProcessMimePartAsync(MimePart part, TemporarySecureMimeContext mimeContext, Agreement<AS2AgreementProperties> agreement, ProcessAS2MessageResult result, string partnerFingerprint, MimeMessage as2Message, TemporaryFileStreamManager tempFileManager)
        {
            if (part.IsEncrypted())
            {
                try
                {
                    var pkcs7 = part as ApplicationPkcs7Mime;
                    var newEntity = pkcs7.Decrypt(mimeContext, tempFileManager.NewStream());
                    _logger.LogCustom(new MessageDecryptedEvent(agreement, as2Message.MessageId));
                    await HandleMimeEntityAsync(newEntity, mimeContext, agreement, result, partnerFingerprint, as2Message, tempFileManager);
                    return;
                }
                catch (Exception ex)
                {
                    _logger.LogError(AS2ProcessorStatusMessages.DecryptionFailed, ex);
                    result.DispositionMessage = AS2ProcessorStatusMessages.DecryptionFailed;
                }
            }

            if (part.IsCompressed())
            {
                try
                {
                    var pkcs7 = part as ApplicationPkcs7Mime;
                    var newEntity = pkcs7.Decompress(mimeContext, tempFileManager.NewStream());
                    _logger.LogCustom(new MessageDecompressedEvent(agreement, as2Message.MessageId));
                    await HandleMimeEntityAsync(newEntity, mimeContext, agreement, result, partnerFingerprint, as2Message, tempFileManager);
                    return;
                }
                catch (Exception ex)
                {
                    _logger.LogError(AS2ProcessorStatusMessages.DecompressionFailed, ex);
                    result.DispositionMessage = AS2ProcessorStatusMessages.DecompressionFailed;
                }
            }

            if (part.IsSignature())
            {
                return; 
            }

            if (part.IsAttachment)
            {
                var fileName = string.IsNullOrWhiteSpace(part.FileName) ? Path.GetTempFileName() : part.FileName;

                //    // TODO: handle base 64 encoded data
                var uploadStreamResult = _fileSystemService.GetUploadStream(fileName, agreement);
                if (uploadStreamResult.IsValid)
                {
                    using (var uploadStream = uploadStreamResult.Payload)
                    {
                        part.ContentObject.DecodeTo(uploadStreamResult.Payload);
                        _logger.LogCustom(new FileReceivedEvent(agreement, fileName, part.ContentObject.Stream.Length, as2Message.MessageId));
                    }
                    await _relayRepository.QueueRelayMessageAsync(new RelayMessage(agreement.AgreementId, agreement.TradingPartner.TradingPartnerId, fileName, DateTime.UtcNow));
                }
                else
                {
                    result.DispositionMessage = AS2ProcessorStatusMessages.UnexpectedProcessingError;
                }
            }
        }

        private AS2MessageDispositionNotice BuildMDN(ProcessAS2MessageResult processMessageResult, CertificateBundle certificateBundle, TemporarySecureMimeContext mimeContext, MimeMessage as2Message)
        {
            var newMessageId = MimeUtils.GenerateMessageId($"{_config.AS2Id}_{as2Message.Headers[AS2HeaderKeys.AS2From]}");
            var result = new AS2MessageDispositionNotice(newMessageId, as2Message.MessageId);

            // Start MDN build
            var mdnDeliveryAddress = as2Message.Headers[AS2HeaderKeys.ReceiptDeliveryOption];
            if (!string.IsNullOrWhiteSpace(mdnDeliveryAddress))
            {
                Uri uriTester = null;
                if (!Uri.TryCreate(mdnDeliveryAddress.Replace("localhost", "127.0.0.1"), UriKind.Absolute, out uriTester))
                    throw new Exception($"The Deposition Notification To uri: {mdnDeliveryAddress} is not a valid uri");
                result.ReceiptDeliveryOption = uriTester;
            }

            var signer = new CmsSigner(new MemoryStream(certificateBundle.Certificate), certificateBundle.CertificatePassword);
            signer.DigestAlgorithm = DIGESTALGORITHM;

            var report = new Multipart("report");
            report.ContentType.Parameters.Add("report-type", "disposition-notification");

            var mdnText = $"The message {as2Message.MessageId} to {as2Message.Headers[AS2HeaderKeys.AS2To]} with subject {as2Message.Headers[HeaderId.Subject]} has been processed. This does not guarantee that the message has been read or understood.\r\n";
            var textPart = new TextPart("plain");
            textPart.ContentObject = new ContentObject(new MemoryStream(Encoding.UTF8.GetBytes(mdnText)));
            textPart.ContentTransferEncoding = ContentEncoding.SevenBit;

            report.Add(textPart);

            var mdnBuilder = new StringBuilder();
            mdnBuilder.AppendLine($"Original-Recipient: rfc822;{as2Message.Headers[AS2HeaderKeys.AS2To]}");
            mdnBuilder.AppendLine($"Final-Recipient: rfc822;{as2Message.Headers[AS2HeaderKeys.AS2To]}");
            mdnBuilder.AppendLine($"Original-Message-ID: {as2Message.Headers[HeaderId.MessageId]}");

            mdnBuilder.AppendLine($"Disposition: automatic-action/MDN-sent-automatically; {processMessageResult.DispositionMessage}");

            // Send back the calculated MIC and use the same MICAlg as we were sent in the original message
            mdnBuilder.AppendLine($"Received-Content-MIC: {processMessageResult.MessageIntegrityCheck}, {processMessageResult.MICAlg}");

            var mdn = new MessageDispositionNotification();
            mdn.ContentObject = new ContentObject(new MemoryStream(Encoding.UTF8.GetBytes(mdnBuilder.ToString())));
            mdn.ContentTransferEncoding = ContentEncoding.SevenBit;

            report.Add(mdn);

            var signed = MultipartSigned.Create(mimeContext, signer, report);

            result.ContentType = signed.ContentType.ToString().Replace("Content-Type: ", "");
            result.AS2To = as2Message.Headers[AS2HeaderKeys.AS2From];

            using (var signatureStream = new MemoryStream())
            {
                signed.WriteTo(signatureStream, true);
                result.Signature = signatureStream.ToArray();
            }

            return result;
        }
    }

    public static class MimeUtility
    {
        public static bool IsEncrypted(this MimePart mimePart)
        {
            if (mimePart.ContentType.IsMimeType("application", "pkcs7-mime") && (mimePart as ApplicationPkcs7Mime).SecureMimeType == SecureMimeType.EnvelopedData)
                return true;

            return false;
        }

        public static bool IsCompressed(this MimePart mimePart)
        {
            if (mimePart.ContentType.IsMimeType("application", "pkcs7-mime") && (mimePart as ApplicationPkcs7Mime).SecureMimeType == SecureMimeType.CompressedData)
                return true;

            return false;
        }

        public static bool IsSignature(this MimePart mimePart)
        {
            if (mimePart.ContentType.IsMimeType("application", "pkcs7-signature"))
                return true;

            return false;
        }

        public static string GetMICAlg(string dispositionNotificationOptionsString)
        {
            string micalg = null;
            if (!string.IsNullOrWhiteSpace(dispositionNotificationOptionsString))
            {
                var match = RegexLibrary.MICAlgRegex.Match(dispositionNotificationOptionsString.RemoveWhitespace());
                if (match.Success && match.Groups.Count > 1 && match.Groups[1].Success)
                {
                    micalg = match.Groups[1].Value;
                }
            }
            return micalg;
        }

        public static MimeEntity Decrypt(this ApplicationPkcs7Mime pkcs7, SecureMimeContext ctx, Stream output)
        {
            if (ctx == null)
                throw new ArgumentNullException(nameof(ctx));

            if (pkcs7.SecureMimeType != SecureMimeType.EnvelopedData)
                throw new InvalidOperationException();

            ctx.DecryptTo(pkcs7.ContentObject.Open(), output);

            output.Seek(0, SeekOrigin.Begin);

            return MimeEntity.Load(output, true);
        }

        public static MimeEntity Decompress(this ApplicationPkcs7Mime pkcs7, SecureMimeContext ctx, Stream output)
        {
            if (ctx == null)
                throw new ArgumentNullException(nameof(ctx));

            if (pkcs7.SecureMimeType != SecureMimeType.CompressedData)
                throw new InvalidOperationException();

            ctx.DecompressTo(pkcs7.ContentObject.Open(), output);

            output.Seek(0, SeekOrigin.Begin);

            return MimeEntity.Load(output, true);
        }

    }
}
