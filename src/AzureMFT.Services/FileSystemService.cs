﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Result;
using AzureMFT.Common.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Services
{
    public class FileSystemService
    {
        private readonly IFileSystemRepository _fileSystemRepository;

        public FileSystemService(IFileSystemRepository fileSystemRepository)
        {
            _fileSystemRepository = fileSystemRepository;
        }

        public PathListingItem GetFileAttributes(string path, IAgreement agreement)
        {
            return _fileSystemRepository.GetFileAttributes(path, agreement);
        }

        public IEnumerable<PathListingItem> GetPathListing(string path, IAgreement agreement)
        {
            return _fileSystemRepository.GetPathListing(path, agreement);
        }

        public GenericResult<Stream> GetDownloadStream(string path, IAgreement agreement)
        {
            return _fileSystemRepository.GetDownloadStream(path, agreement);
        }

        public GenericResult<Stream> GetUploadStream(string path, IAgreement agreement)
        {
            return _fileSystemRepository.GetUploadStream(path, agreement);
        }

        public GenericResult<Stream> GetUploadStream(string path, string subFolder, IAgreement agreement)
        {
            return _fileSystemRepository.GetUploadStream(path, subFolder, agreement);
        }

        public ValidationResult RenameFile(string sourcePath, string targetPath, IAgreement agreement)
        {
            return _fileSystemRepository.RenameFile(sourcePath, targetPath, agreement);
        }

        public ValidationResult DeleteFile(string path, IAgreement agreement)
        {
            return _fileSystemRepository.DeleteFile(path, agreement);
        }
    }
}
