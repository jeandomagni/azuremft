﻿using AzureMFT.Common.Cache;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Services;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Services
{
    public class CacheService : ICacheService
    {
        private readonly ICacheRepository _repository;

        public CacheService(ICacheRepository repository)
        {
            _repository = repository;
        }

        public byte[] Get(string key)
        {
            return _repository.Get(key);
        }

        public T Get<T>(string key)
        {
            byte[] data = Get(key);
            if (data != null)
            {
                using (var memoryStream = new MemoryStream(data))
                {
                    return Serializer.Deserialize<T>(memoryStream);
                }
            }
            else
                return default(T);
        }

        public bool Remove(string key)
        {
            return _repository.Remove(key);
        }

        public void Set(string key, byte[] value, TimeSpan expiresIn)
        {
            _repository.Set(key, value, new DistributedCacheEntryOptions() { AbsoluteExpirationRelativeToNow = expiresIn });
        }

        public void Set<T>(string key, T value, TimeSpan expiresIn)
        {
            if (value != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    Serializer.Serialize(memoryStream, value);
                    Set(key, memoryStream.ToArray(), expiresIn);
                }
            }
        }
    }
}
