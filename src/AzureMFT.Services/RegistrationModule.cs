﻿using Autofac;
using AzureMFT.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Services
{
    public class RegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<AgreementService>().SingleInstance();
            builder.RegisterType<FileSystemService>().SingleInstance();
            builder.RegisterType<CertificateService>().As<ICertificateService>().SingleInstance();
            builder.RegisterType<AS2ProcessingService>().SingleInstance();
            builder.RegisterType<CacheService>().As<ICacheService>().SingleInstance();
        }
    }
}
