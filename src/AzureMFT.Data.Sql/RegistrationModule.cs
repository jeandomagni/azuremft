﻿using Autofac;
using AzureMFT.Common.Repositories;
using AzureMFT.Data.Sql.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Data.Sql
{
    public class RegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().SingleInstance();
            builder.RegisterType<AgreementRepository>().As<IAgreementRepository>().SingleInstance();
            builder.RegisterType<BanListRepository>().As<IBanListRepository>().SingleInstance();
        }
    }
}
