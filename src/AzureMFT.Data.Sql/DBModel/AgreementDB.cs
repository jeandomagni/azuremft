﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Data.Sql.DBModel
{
    public class AgreementDB
    {
        public int AgreementId { get; set; }
        public string AgreementName { get; set; }
        public int AgreementType { get; set; }
        public int TradingPartnerId { get; set; }
        public string TradingPartnerName { get; set; }
        public DateTime CreatedUtcDate { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime? LastUsedUtcDate { get; set; }
        public int FailedAuthenticationAttemptCount { get; set; }
        public bool LockedOut { get; set; }
        public DateTime? LastLockedOutUtcDate { get; set; }

        public byte[] AgreementProperties { get; set; }
    }
}
