﻿using AzureMFT.Common.Models;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Common.Models.SFTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace AzureMFT.Data.Sql.DBModel
{
    public static class AgreementDBModelFactory
    {
        public static AgreementDB Create<T>(Agreement<T> agreement)
        {
            var db = new AgreementDB();
            db.AgreementId = agreement.AgreementId;
            db.AgreementName = agreement.AgreementName;
            db.AgreementType = (byte)agreement.AgreementType;
            db.TradingPartnerId = agreement.TradingPartner.TradingPartnerId;
            db.TradingPartnerName = agreement.TradingPartner.TradingPartnerName;
            db.CreatedUtcDate = agreement.CreatedUtcDate;
            db.CreatedByUserId = agreement.CreatedByUserId;
            db.LastUsedUtcDate = agreement.LastUsedUtcDate;
            db.LockedOut = agreement.LockedOut;
            db.LastLockedOutUtcDate = agreement.LastLockedOutUtcDate;

            using (var ms = new MemoryStream())
            {
                Serializer.Serialize<T>(ms, agreement.AgreementProperties);
                db.AgreementProperties = ms.ToArray();
            }

            return db;
        }

        public static Agreement<T> Create<T>(AgreementDB agreementDB)
        {
            if (agreementDB == null)
                return null;

            var model = new Agreement<T>();
            model.AgreementId = agreementDB.AgreementId;
            model.AgreementName = agreementDB.AgreementName;
            model.AgreementType = (AgreementType)agreementDB.AgreementType;
            model.TradingPartner = new TradingPartner()
            {
                TradingPartnerId = agreementDB.TradingPartnerId,
                TradingPartnerName = agreementDB.TradingPartnerName
            };
            model.CreatedUtcDate = agreementDB.CreatedUtcDate;
            model.CreatedByUserId = agreementDB.CreatedByUserId;
            model.LastUsedUtcDate = agreementDB.LastUsedUtcDate;
            model.LockedOut = agreementDB.LockedOut;
            model.LastLockedOutUtcDate = agreementDB.LastLockedOutUtcDate;

            using (var ms = new MemoryStream(agreementDB.AgreementProperties))
            {
                model.AgreementProperties = Serializer.Deserialize<T>(ms);
            }

            return model;
        }
    }
}
