﻿using AzureMFT.Common.Configuration;
using AzureMFT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Data.Sql
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private Database database;
        private readonly string connectionString;

        public DatabaseFactory(ISecretConfigurationManager secretConfigurationManager)
        {
            connectionString = secretConfigurationManager.GetSecret(Secrets.Names.SqlConnectionString);
        }

        public Database GetDatabase()
        {
            if (database == null)
                database = new Database(connectionString);

            return database;
        }
    }
}
