﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Data.Sql
{
    public interface IDatabaseFactory
    {
        Database GetDatabase();
    }
}
