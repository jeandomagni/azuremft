﻿using AzureMFT.Data.Sql.Retry;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Data.Sql
{
    public class Database
    {
        public string ConnectionString { get; private set; }

        public Database(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public SqlConnection GetConnection()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            var connectivityRetryPolicy = SqlRetryUtility.GetNetworkConnectivityErrorRetryPolicy();

            SqlRetryUtility.GetVeryShortRunningRetryPolicy().ExecuteAction(() =>
            {
                connectivityRetryPolicy.ExecuteAction(() =>
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();
                });
            });

            return connection;
        }
    }
}
