﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Data.Sql.Retry
{
    /// <summary>
    /// Implements a strategy that detects network connectivity errors such as "host not found".
    /// </summary>
    public class NetworkConnectivityErrorDetectionStrategy : ITransientErrorDetectionStrategy
    {
        public bool IsTransient(Exception ex)
        {
            SqlException sqlEx = ex as SqlException;
            if (sqlEx != null)
            {
                int number = sqlEx.Number;
                if (number == 11001)
                    return true;
            }
            return false;
        }
    }
}
