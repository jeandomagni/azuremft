﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Data.Sql.Retry
{
    public static class SqlRetryUtility
    {
        private static RetryStrategy shortRunningStrategy;
        private static RetryStrategy veryShortRunningStrategy;

        static SqlRetryUtility()
        {
            shortRunningStrategy = new ExponentialBackoff(6, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(1));
            veryShortRunningStrategy = new Incremental(3, TimeSpan.FromMilliseconds(50), TimeSpan.FromMilliseconds(50));
        }

        /// <summary>
        /// Max cumulative wait time of about 4 minutes (10 retries with exponential backoffs growing from 1 to 30 seconds)
        /// </summary>
        public static RetryPolicy GetLongRunningRetryPolicy()
        {
            return new RetryPolicy<SqlDatabaseTransientErrorDetectionStrategy>(RetryStrategy.DefaultExponential);
        }

        /// <summary>
        /// Max cumulative wait time of about 30 seconds (6 retries with exponential backoffs growing from 1 to 10 seconds)
        /// </summary>
        public static RetryPolicy GetShortRunningRetryPolicy()
        {
            return new RetryPolicy<SqlDatabaseTransientErrorDetectionStrategy>(shortRunningStrategy);
        }

        /// <summary>
        /// Max cumulative wait time of about 200 milliseconds (3 retries with an incremental backoff fo 50ms)
        /// </summary>
        public static RetryPolicy GetVeryShortRunningRetryPolicy()
        {
            return new RetryPolicy<SqlDatabaseTransientErrorDetectionStrategy>(veryShortRunningStrategy);
        }

        /// <summary>
        /// Should only be used when opening sql connections
        /// </summary>
        public static RetryPolicy GetNetworkConnectivityErrorRetryPolicy()
        {
            return new RetryPolicy<NetworkConnectivityErrorDetectionStrategy>(1, TimeSpan.FromMilliseconds(1.0));
        }
    }
}
