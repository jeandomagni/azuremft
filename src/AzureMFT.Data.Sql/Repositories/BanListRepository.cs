﻿using AzureMFT.Common.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace AzureMFT.Data.Sql.Repositories
{
    public class BanListRepository : RepositoryBase, IBanListRepository
    {
        public BanListRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }

        public void AddToBanList(string ipAddress)
        {
            using (var conn = Database.GetConnection())
            {
                conn.Execute(@"INSERT INTO MFTBannedIPs(IPAddress, AddedUtcDate)
                               VALUES(@ipAddress, @addedUtcDate)",
                new { ipAddress = ipAddress, addedUtcDate = DateTime.UtcNow });
            }
        }

        public HashSet<string> GetBanList()
        {
            using (var conn = Database.GetConnection())
            {
                return new HashSet<string>(conn.Query<string>(@"SELECT * from MFTBannedIPs").Distinct());
            }
        }
    }
}
