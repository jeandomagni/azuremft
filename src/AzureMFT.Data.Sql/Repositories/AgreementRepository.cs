﻿using System;
using System.Linq;
using Dapper;
using AzureMFT.Common.Models;
using AzureMFT.Common.Repositories;
using AzureMFT.Data.Sql.DBModel;
using AzureMFT.Common.Models.SFTP;
using AzureMFT.Common.Models.AS2;

namespace AzureMFT.Data.Sql.Repositories
{
    public class AgreementRepository : RepositoryBase, IAgreementRepository
    {
        public AgreementRepository(IDatabaseFactory factory) : base(factory)
        {

        }

        public void CreateAgreement<T>(Agreement<T> agreement)
        {
            using (var conn = Database.GetConnection())
            {
                conn.Execute(@"
                    INSERT INTO MFTAgreements(AgreementName, AgreementType, TradingPartnerId, CreatedUtcDate, CreatedByUserId, LastUsedUtcDate, FailedAuthenticationAttemptCount, LockedOut, LastLockedOutUtcDate, AgreementProperties)
                    VALUES(@AgreementName, @AgreementType, @TradingPartnerId, @CreatedUtcDate, @CreatedByUserId, @LastUsedUtcDate, @FailedAuthenticationAttemptCount, @LockedOut, @LastLockedOutUtcDate, @AgreementProperties)
                ", AgreementDBModelFactory.Create(agreement));
            }
        }

        public Agreement<T> GetAgreement<T>(int agreementId)
        {
            using (var conn = Database.GetConnection())
            {
                var agreementDB = conn.Query<AgreementDB>(@"
                    SELECT a.*, t.* FROM MFTAgreements a
                    INNER JOIN MFTTradingPartners t
                        ON t.TradingPartnerId = a.TradingPartnerId
                    WHERE a.AgreementId = @agreementId
                ", new
                {
                    agreementId = agreementId
                }).SingleOrDefault();

                return AgreementDBModelFactory.Create<T>(agreementDB);
            }
        }

        public Agreement<T> GetAgreement<T>(string agreementName, AgreementType agreementType)
        {
            using (var conn = Database.GetConnection())
            {
                var agreementDB = conn.Query<AgreementDB>(@"
                    SELECT a.*, t.* FROM MFTAgreements a
                    INNER JOIN MFTTradingPartners t
                        ON t.TradingPartnerId = a.TradingPartnerId
                    WHERE AgreementName = @agreementName
                        AND AgreementType = @agreementType
                ", new
                {
                    agreementName = agreementName,
                    agreementType = (byte)agreementType
                }).SingleOrDefault();

                return AgreementDBModelFactory.Create<T>(agreementDB);
            }
        }

        public void LogSuccessfulAuthentication(string agreementName, DateTime lastUsedUtcDate, int failedAuthenticationAttemptCount)
        {
            using (var conn = Database.GetConnection())
            {
                conn.Execute(@"
                    UPDATE MFTAgreements
                        SET LastUsedUtcDate = @lastUsedUtcDate,
                            FailedAuthenticationAttemptCount = @failedAuthenticationAttemptCount
                    WHERE AgreementName = @agreementName
                ", new
                {
                    lastUsedUtcDate = lastUsedUtcDate,
                    failedAuthenticationAttemptCount = failedAuthenticationAttemptCount,
                    agreementName = agreementName
                });
            }
        }

        public void LogFailedAuthentication(string agreementName, int failedAuthenticationAttemptCount, bool lockedOut, DateTime? lastLockedOutUtcDate)
        {
            using (var conn = Database.GetConnection())
            {
                conn.Execute(@"
                    UPDATE MFTAgreements
                        SET FailedAuthenticationAttemptCount = @failedAuthenticationAttemptCount,
                            LockedOut = @lockedOut,
                            LastLockedOutUtcDate = @lastLockedOutUtcDate
                    WHERE AgreementName = @agreementName
                ", new
                {
                    failedAuthenticationAttemptCount = failedAuthenticationAttemptCount,
                    lockedOut = lockedOut,
                    lastLockedOutUtcDate = lastLockedOutUtcDate,
                    agreementName = agreementName
                });
            }
        }

        public void UnlockAgreement(int agreementId)
        {
            using (var conn = Database.GetConnection())
            {
                conn.Execute(@"
                    UPDATE MFTAgreements
                        SET LockedOut = 0,
                            FailedAuthenticationAttemptCount = 0
                    WHERE AgreementId = @agreementId
                ", new { agreementId = agreementId });
            }
        }

        public void UpdateAgreement<T>(Agreement<T> agreement)
        {
            using (var conn = Database.GetConnection())
            {
                conn.Execute(@"
                    UPDATE MFTAgreements
                    SET AgreementName = @AgreementName,
                        AgreementType = @AgreementType,
                        TradingPartnerId = @TradingPartnerId,
                        CreatedUtcDate = @CreatedUtcDate,
                        CreatedByUserId = @CreatedByUserId,
                        LastUsedUtcDate = @LastUsedUtcDate,
                        FailedAuthenticationAttemptCount = @FailedAuthenticationAttemptCount,
                        LockedOut = @LockedOut,
                        LastLockedOutUtcDate = @LastLockedOutUtcDate,
                        AgreementProperties = @AgreementProperties
                    WHERE AgreementId = @AgreementId"
                , AgreementDBModelFactory.Create(agreement));
            }
        }
    }
}
