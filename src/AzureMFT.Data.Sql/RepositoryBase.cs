﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureMFT.Data.Sql
{
    public class RepositoryBase
    {
        public Database Database { get; set; }

        public RepositoryBase(IDatabaseFactory databaseFactory)
        {
            Database = databaseFactory.GetDatabase();
        }

        public DbString GetAnsiString(string value, int length)
        {
            return new DbString() { Value = value, IsFixedLength = true, Length = length, IsAnsi = true };
        }
    }
}
