using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.ServiceRuntime;
using AzureMFT.Server;
using Autofac;
using AzureMFT.Common;
using AzureMFT.WindowsAzure;
using AzureMFT.Common.Logging;
using System.IO;
using AzureMFT.Common.Configuration;
using Microsoft.Azure;
using AzureMFT.Common.Repositories;
using AzureMFT.Common.Models;
using Newtonsoft.Json;
using AzureMFT.Common.Services;
using AzureMFT.Services;
using AzureMFT.Common.Models.AS2;
using AzureMFT.Server.AS2;

namespace AzureMFT.Worker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);
        private IEnumerable<IMFTService> _services = null;
        private ILoggingService _logger = null;

        public override void Run()
        {
            Trace.TraceInformation("AzureMFT.Worker is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 100;

            try
            {
                var container = RegisterIOC();
                _services = container.Resolve<IEnumerable<IMFTService>>();
                _logger = container.Resolve<ILoggingService>();

                var endpoints = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints.ToDictionary(x => x.Key, x => x.Value.IPEndpoint);

                foreach (var service in _services)
                {
                    service.Start(container, endpoints);
                }
            }
            catch(Exception ex)
            {
                File.WriteAllText(Path.Combine(Path.GetTempPath(), Path.GetTempFileName()), ex.ToString());
                if(_logger != null)
                    _logger.LogException(ex);
            }


            return base.OnStart();
        }

        public override void OnStop()
        {
            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            if(_services != null)
            {
                foreach (var service in _services.OrderBy(x => x.ShutdownOrder))
                {
                    service.Stop();
                }
            }

            if (_logger != null)
            {
                _logger.LogCriticalInformation("Logger flushed");
                _logger.Flush();
            }

            base.OnStop();
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(1000);
            }
        }

        private static IContainer RegisterIOC()
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(DIRegistrationHelper.GetRegistrationAssemblies());

            var config = GetConfiguration();
            builder.RegisterInstance(config).SingleInstance();

            if(config.Environment.IsDevelopment())
            {
                builder.RegisterType<DevRelayRepository>().As<IRelayRepository>().SingleInstance();

                var thumbprint = System.Configuration.ConfigurationManager.AppSettings["DevRootCertificateThumbprint"];
                var password = System.Configuration.ConfigurationManager.AppSettings["DevRootCertificatePassword"];
                builder.RegisterType<LocalCertificateService>().As<ICertificateService>()
                                                               .WithParameter("rootCertificateThumbprint", thumbprint)
                                                               .WithParameter("rootCertificatePassword", password);

                // TODO: Remove this
                builder.RegisterType<LocalAS2MDNService>().As<IMFTService>().SingleInstance();

                builder.RegisterType<AppSettingSecretConfigurationManager>().As<ISecretConfigurationManager>().SingleInstance();
                builder.RegisterType<MemoryCacheRepository>().As<ICacheRepository>().SingleInstance();
            }
            else
            {
                // TODO: Fix this by deleting the LocalAS2MDNService and extracting the service bus stuff so that it can be swapped out
                builder.RegisterType<AS2MDNService>().As<IMFTService>().SingleInstance();
            }

            // Runner specific registrations
            builder.RegisterType<ApplicationInsightsLogger>().As<ILoggingService>().SingleInstance();

            return builder.Build();
        }

        private static ConfigurationManager GetConfiguration()
        {
            var builder = new ConfigurationManagerBuilder();

            builder.ADClientId = CloudConfigurationManager.GetSetting("ADClientId", false);
            builder.ADClientSecret = CloudConfigurationManager.GetSetting("ADClientSecret", false);
            builder.ApplicationInsightsInstrumentationKey = CloudConfigurationManager.GetSetting("ApplicationInsightsInstrumentationKey");
            builder.AS2FileReceivedQueueName = CloudConfigurationManager.GetSetting("AS2FileReceivedQueueName");
            builder.AS2Id = CloudConfigurationManager.GetSetting("AS2Id");
            builder.AS2MDNDispatchQueueName = CloudConfigurationManager.GetSetting("AS2MDNDispatchQueueName");
            builder.AS2PublicCertificateAlias = CloudConfigurationManager.GetSetting("AS2PublicCertificateAlias");
            builder.AS2PublicCertificateKey = CloudConfigurationManager.GetSetting("AS2PublicCertificateKey");
            builder.AS2PublicCertificatePasswordKey = CloudConfigurationManager.GetSetting("AS2PublicCertificatePasswordKey");
            builder.AS2PublicCertificateVersion = CloudConfigurationManager.GetSetting("AS2PublicCertificateVersion");
            builder.Environment = new HostingEnvironment(CloudConfigurationManager.GetSetting("Environment"));
            builder.FailedAuthenticationAttemptLimit = int.Parse(CloudConfigurationManager.GetSetting("FailedAuthenticationAttemptLimit"));
            builder.LogLevel = (Common.Logging.LogLevel)Enum.Parse(typeof(Common.Logging.LogLevel), CloudConfigurationManager.GetSetting("LogLevel"));
            builder.RelayQueueName = CloudConfigurationManager.GetSetting("RelayQueueName");
            builder.SFTPHostCertificateKey = CloudConfigurationManager.GetSetting("SFTPHostCertificateKey");
            builder.SFTPHostCertificatePasswordKey = CloudConfigurationManager.GetSetting("SFTPHostCertificatePasswordKey");
            builder.SFTPHostCertificateVersion = CloudConfigurationManager.GetSetting("SFTPHostCertificateVersion");
            builder.TempPath = RoleEnvironment.GetLocalResource("as2temp").RootPath;
            builder.VaultName = CloudConfigurationManager.GetSetting("VaultName");

            return builder.Build();
        }
    }

    public class DevRelayRepository : IRelayRepository
    {
        public Task QueueAS2FileReceivedMessageAsync(RelayMessage queueMessage)
        {
            return Task.Run(() =>
            {

            });
        }

        public Task QueueAS2MDNMessageAsync(AS2MessageDispositionNotice queueMessage)
        {
            return Task.Run(() =>
            {

            });
        }

        public Task QueueRelayMessageAsync(RelayMessage message)
        {
            return Task.Run(() =>
            {
                File.WriteAllText(Path.Combine(Path.GetTempPath(), Path.GetTempFileName()), JsonConvert.SerializeObject(message));
            });
        }
    }
}
