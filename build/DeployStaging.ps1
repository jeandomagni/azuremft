$ErrorActionPreference = "Stop"
$managementthumbprint = $args[0]
$cert = Get-Item cert:\LocalMachine\My\$managementthumbprint

$sub = $args[1]
$servicename = $args[2]
$storageaccountname = $args[3]
$artifactpath = "Artifacts"
$package = join-path $artifactpath $args[4]
$config = join-path $artifactpath $args[5]
$buildLabel = $args[6]

if ((Get-Module | ?{$_.Name -eq "Azure"}) -eq $null)
{
    Import-Module "C:\Program Files (x86)\Microsoft SDKs\Azure\PowerShell\ServiceManagement\Azure\Azure.psd1"
}

Write-Host Setting Subscription...
Set-AzureSubscription -SubscriptionName "Supplylogix" -SubscriptionId $sub -Certificate $cert -CurrentStorageAccount $storageaccountname

Write-Host Selecting subscription...
Select-AzureSubscription -SubscriptionName "Supplylogix"

Write-Host Getting service...
$deployment = Get-AzureDeployment -Slot Staging -ServiceName $servicename -ErrorAction silentlycontinue

if($deployment.Status -ne $null)
{
    Write-Host Found existing staging deployment

    Write-Host Running in-place upgrade...
    Set-AzureDeployment -Upgrade -Slot Staging -ServiceName $servicename -Package $package -Configuration $config -Label $buildLabel -Mode Auto -Force -ErrorAction Stop
}
else
{
	Write-Host Existing staging deployment not found

    Write-Host Creating new staging deployment...
	New-AzureDeployment -Slot Staging -ServiceName $servicename -Package $package -Configuration $config -Label $buildLabel -ErrorAction Stop
}

Write-Host Done!